"""
Urls for:

- Oauth2 extensions for app self-registration and verification
- Overriding oauth2_provider's default urls with those Mastodon-compatible clients expect.

"""
from django.conf.urls import url
from oauth2_provider import views as oauth2_views

from wonambi.utils.router import OptionalSlashRouter

from oauth.api import OauthApplicationViewset

router = OptionalSlashRouter()
router.register(r'apps', OauthApplicationViewset)

app_name = "oauth2_provider"


base_urlpatterns = [
    url(r"^authorize/$", oauth2_views.AuthorizationView.as_view(), name="authorize"),
    url(r"^token/$", oauth2_views.TokenView.as_view(), name="token"),
# Original URL from oauth2_provider:
#    url(r"^revoke_token/$", views.RevokeTokenView.as_view(), name="revoke-token"),
# Mastodon-expected version:
    url(r"^revoke/$", oauth2_views.RevokeTokenView.as_view(), name="revoke-token"),
    url(r"^introspect/$", oauth2_views.IntrospectTokenView.as_view(), name="introspect"),
]


# This section from oauth2_provider.urls, but we're (probably) not using it
#management_urlpatterns = [
#    # Application management views
#    url(r"^applications/$", views.ApplicationList.as_view(), name="list"),
#    url(r"^applications/register/$", views.ApplicationRegistration.as_view(), name="register"),
#    url(r"^applications/(?P<pk>[\w-]+)/$", views.ApplicationDetail.as_view(), name="detail"),
#    url(r"^applications/(?P<pk>[\w-]+)/delete/$", views.ApplicationDelete.as_view(), name="delete"),
#    url(r"^applications/(?P<pk>[\w-]+)/update/$", views.ApplicationUpdate.as_view(), name="update"),
#    # Token management views
#    url(r"^authorized_tokens/$", views.AuthorizedTokensListView.as_view(), name="authorized-token-list"),
#    url(r"^authorized_tokens/(?P<pk>[\w-]+)/delete/$", views.AuthorizedTokenDeleteView.as_view(),
#        name="authorized-token-delete"),
#]


# urlpatterns = base_urlpatterns + management_urlpatterns

# final urlpatterns, for inclusion in wonambi.urls
# note that router.urls is included separately, under a different url path stem (/api/v1)
urlpatterns = base_urlpatterns
