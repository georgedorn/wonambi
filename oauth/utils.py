import json

from oauth2_provider.models import get_grant_model
from oauth2_provider.views.base import TokenView
from oauthlib import common


def create_access_token(request):
    """
    Helper method to create a valid AccessToken to pass to the frontend for auth purposes.

    Passes the buck upstream to django-oauth-toolkit, which in turn passes the buck to oauthlib.

    Kludge together from oauth2_provider.oauth2_validators.OAuth2Validator.save_bearer_token and
    _create_access_token
    """
    Grant = get_grant_model()
    try:
        grant = Grant.objects.get(user=request.user)
    except Grant.DoesNotExist:
        return None
    fake_request_body = {'client_id': grant.application.client_id,
                         'client_secret': grant.application.client_secret,
                         'grant_type': 'authorization_code',
                         'redirect_uri': grant.application.redirect_uris.split(' ')[0],
                         'code': grant.code
                         }
    new_request = request
    new_request.method = 'POST'
    new_request.POST = request.GET.copy()
    new_request.POST.update(fake_request_body)

    # Remove anything that oauthlib might object to.  This is a moving target.
    new_request.GET = new_request.META['QUERY_STRING'] = {}
    
    res = TokenView().post(new_request)
    body = json.loads(res.content)  # TODO: detect errors like code expiration
    return body.get('access_token')
