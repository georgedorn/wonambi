"""
API to manage Oauth apps and other endpoints not already provided by django-oauth-toolkit
"""

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import serializers
from rest_framework import exceptions
from rest_framework.decorators import action
from rest_framework.reverse import reverse

from oauth.models import OauthApplication


class OauthApplicationSerializer(serializers.ModelSerializer):
    """
    Handle defaults and Mastodon's broken behavior of acception 'client_name' but returning 'name'.
    """
    name = serializers.CharField(required=True)
    client_name = serializers.CharField(source='name', required=False, read_only=True)
    client_type = serializers.CharField(default='confidential')
    authorization_grant_type = serializers.CharField(default=OauthApplication.GRANT_AUTHORIZATION_CODE)
    redirect_uris = serializers.CharField(required=True)

    class Meta:
        model = OauthApplication
        fields = ('id', 'client_id', 'redirect_uris', 'client_type', 'authorization_grant_type',
                  'client_secret', 'name', 'client_name', 'skip_authorization', 'created', 'updated',
                  'scopes', 'website')

    def __init__(self, instance=None, data=None, **kwargs):
        """
        Override base init to allow `client_name` to be used instead of `name`.
        Because Mastodon.
        """
        if data is None:
            data = {}
        if data.get('client_name') and not data.get('name'):
            data['name'] = data['client_name']
        if not data.get('redirect_uris'):
            request = kwargs.get('context', {}).get('request', None)
            if request:
                data['redirect_uris'] = request.build_absolute_uri(reverse('mastodon_home'))
        
        return super().__init__(instance, data, **kwargs)


class OauthApplicationViewset(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = OauthApplication.objects.all()  # TODO: get rid of this entirely if possible?
    serializer_class = OauthApplicationSerializer

    def get_serializer_context(self):
        return {'request': self.request}

    def create(self, request, *args, **kwargs):
        """
        Wide-open create endpoint, to allow apps to self-register.
        """
        if 'id' in request.data or 'pk' in request.data:
            raise exceptions.MethodNotAllowed("Only new apps may be created at this endpoint")
        return super().create(request, *args, **kwargs)

    @action(detail=False, methods=['get'])
    def verify_credentials(self, request, *args, **kwargs):
        """
        Verifies that the oauth credentials for an app are correct.
        
        @TODO: There might be a way to get oauth-toolkit to do this for us?
        @TODO: Do we even need this?
        """
        