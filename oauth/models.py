from django.db import models

from oauth2_provider.models import AbstractApplication
from oauth2_provider.scopes import get_scopes_backend

default_scopes = ' '.join(get_scopes_backend().get_default_scopes())

class OauthApplication(AbstractApplication):
    scopes = models.CharField(max_length=1024, default=default_scopes)
    website = models.URLField(null=True, default='')
