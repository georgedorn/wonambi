from traceback import extract_stack, format_list
from unittest import mock
from rest_framework.test import APIClient
from django.http.response import HttpResponseBase
from oauth.models import OauthApplication

from social.tests import SocialTestCase

# hack to get traceback when a response has an unexpected code
# see https://stackoverflow.com/a/32982100/402605
# can also get traceback out of res._init_stack directly
orig_response_init = HttpResponseBase.__init__

def new_response_init(self, *args, **kwargs):
    orig_response_init(self, *args, **kwargs)
    self._init_stack = extract_stack()


class ResponseTracebackTestCase(SocialTestCase):
    @classmethod
    def setUpClass(cls):
        cls.patcher = mock.patch.object(HttpResponseBase, '__init__', new_response_init)
        cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.patcher.stop()

    def assertResponseCodeEquals(self, response, status_code=200):
        self.assertEqual(response.status_code, status_code,
            "Response code was '%s', expected '%s'" % (
                response.status_code, status_code,
            ) + '\n' + ''.join(format_list(response._init_stack))
        )

class TestOauthAPI(ResponseTracebackTestCase):
    """
    Tests of Mastodon's extension to Oauth
    """
    create_app_url = '/api/v1/apps'
    authorize_url = '/oauth/authorize/'
    token_url = '/oauth/token/'
    revoke_url = '/oauth/revoke/'
    getting_started_url = 'http://testserver/web/getting-started/'
    
    def _get_oauth_token(self):
        application = OauthApplication.objects.create(name='test app', scopes='read write',
                                                      redirect_uris=self.getting_started_url,
                                                      website='http://testserver.com/home',
                                                      authorization_grant_type='authorization-code',
                                                      client_type='confidential'
                                                      )

        # this gets us the authorization form; we're already logged in but have to auth the client
        res = self.client.get(self.authorize_url, {
            'response_type': 'code',
            'client_id': application.client_id,
            'client_secret': application.client_secret,
            'redirect_uri': self.getting_started_url,
            'scope': 'read write'
            }, **{'wsgi.url_scheme': 'https'})
        self.assertResponseCodeEquals(res, 200)

        # Check for no errors
        self.assertEqual(res.context_data.get('error'), None, res.context_data.get('error'))

        # A hacky way to simulate form submission.
        form = res.context_data['form']

        post_data = form.initial.copy()
        post_data['allow'] = True
        res = self.client.post(self.authorize_url, post_data, **{'wsgi.url_scheme': 'https'})

        res = self.client.get(self.getting_started_url, **{'wsgi.url_scheme': 'https'})
        return res.context['initial_state']['meta']['access_token']

    def test_oauth_authentication(self):
        token = self._get_oauth_token()

        self.logout()  # ensure we aren't passing django session cookies

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        
        # GET an API endpoint that requires request.user.person
        res = client.get(self.verify_credentials_url, **{'wsgi.url_scheme': 'https'})
        
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json()['acct'], self.person_a.acct)

    def test_oauth_authentication_fails(self):
        token = self._get_oauth_token() + 'aoeu'  # invalid token

        self.logout()  # ensure we aren't passing django session cookies

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

        # GET an API endpoint that requires request.user.person
        res = client.get(self.verify_credentials_url, **{'wsgi.url_scheme': 'https'})

        self.assertEqual(res.status_code, 401)
    
    def test_create_app(self):
        """
        Test creating (registering) an oauth app.

        Proper oauth provides the client's name as 'name', not 'client_name'.
        """
        res = self._post_json(self.create_app_url,
                              {'name': 'test client',
                               'redirect_uris': self.getting_started_url,
                               'scopes': 'read write',
                               'website': 'http://testserver.com/home'
                               }
                              )
        self.assertEqual(res.status_code, 201)
        payload = res.json()
        self.assertEqual(payload['client_name'], 'test client')
        self.assertEqual(payload['name'], 'test client')

        application = OauthApplication.objects.get(name='test client')
        self.assertEqual(application.scopes, 'read write')
        self.assertEqual(payload['client_id'], application.client_id)
        
        # Check for sane defaults; if this test fails, we upgraded django-oauth-toolkit
        # and the defaults changed, or we bungled something in the oauth settings
        self.assertEqual(application.client_type, 'confidential')
        self.assertEqual(application.authorization_grant_type, 'authorization-code')
        self.assertEqual(application.redirect_uris, self.getting_started_url)
        self.assertEqual(application.client_secret, payload['client_secret'])
        self.assertEqual(application.skip_authorization, False)

    def test_create_app_mastodon(self):
        """
        Test the Mastodon UI's approach to oauth app creation / registration.

        Mastodon sends 'client_name' even though the field is called 'name'.
        Later oauth calls provide this as 'name' as well.
        """
        res = self._post_json(self.create_app_url,
                              {'client_name': 'test client',
                               'redirect_uris': self.getting_started_url,
                               'scopes': 'read write',
                               'website': 'http://testserver.com/home'
                               }
                              )
        self.assertEqual(res.status_code, 201)
        payload = res.json()
        self.assertEqual(payload['client_name'], 'test client')
        self.assertEqual(payload['name'], 'test client')

        application = OauthApplication.objects.get(name='test client')
        self.assertEqual(application.scopes, 'read write')

    def test_create_app_defaults(self):
        res = self._post_json(self.create_app_url, {})
        self.assertEqual(res.status_code, 400)
        payload = res.json()
        self.assertTrue('name' in payload.keys())

    def test_create_only(self):
        """
        Several other verbs are not to be used with the apps endpoint.
        """
        application = OauthApplication.objects.create(name='test app', scopes='read write')
        res = self.client.delete(self.create_app_url, {'id': application.pk})
        self.assertEqual(res.status_code, 405)
        application.refresh_from_db()

        modified_data = {'name': 'test app', 'scopes': 'read write expunge', 'id': application.pk}
        res = self._post_json(self.create_app_url, modified_data)
        self.assertEqual(res.status_code, 405)
        application.refresh_from_db()
        self.assertEqual(application.scopes, 'read write')
        
        small_mod_data = {'scopes': 'read write expunge'}
        res = self._patch_json(self.create_app_url, small_mod_data)
        self.assertEqual(res.status_code, 405)
        application.refresh_from_db()
        self.assertEqual(application.scopes, 'read write')

        # even GET doesn't work
        res = self.client.get(self.create_app_url)
        self.assertEqual(res.status_code, 405)
        payload_keys = list(res.json().keys())
        self.assertEqual(payload_keys, ['detail'])  # only the error returned, no app data

    def test_authorize_mastodon_fe_for_pleroma_workflow(self):
        """
        Test that follows the general pattern for how Pleroma uses the Mastodon FE it is bundled
        with.  Assumes the user is already logged in - another test for not-logged-in state is needed.
        """
        self.logout()  # start logged out - the app login page is the first time the BE knows the user
        res = self._post_json(self.create_app_url,
                      {'client_name': 'test dummy client',
                       'redirect_uris': self.getting_started_url,
                       'scopes': 'read write',
                       }
                      )

        payload = res.json()
        client_id = payload['client_id']
        client_secret = payload['client_secret']
        
        self.login()
        # this gets us the authorization form; we're already logged in but have to auth the client
        res = self.client.get(self.authorize_url, {
            'response_type': 'code',
            'client_id': client_id,
            'client_secret': client_secret,
            'redirect_uri': self.getting_started_url,
            'scope': 'read write'
            })

        # Check for no errors
        self.assertEqual(res.context_data.get('error'), None)

        # A hacky way to simulate form submission.
        form = res.context_data['form']

        post_data = form.initial.copy()
        post_data['allow'] = True
        res = self.client.post(self.authorize_url, post_data)
        self.assertEqual(res.status_code, 302)
        redirect_to = res['Location']

        app = OauthApplication.objects.get()

        grant = app.grant_set.first()
        self.assertTrue('code=%s' % grant.code in redirect_to)
        
        res = self.client.get(redirect_to)

        # test getting the actual bearer token
        # ('client_id', 'grant_type', 'redirect_uri')
        
        initial_state = res.context['initial_state']
        access_token = initial_state['meta']['access_token']

        self.logout()  # ensure we aren't passing django session cookies

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + access_token)
        
        # GET an API endpoint that requires request.user.person
        res = client.get(self.verify_credentials_url)
        
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json()['acct'], self.person_a.acct)
