from django.test.testcases import TestCase

from microblog.models import Status, Favorite
from microblog.serializers import StatusSerializer

from social.models import Person
from unittest.mock import Mock


class TestStatusSerializer(TestCase):

    def test_read_status(self):
        status = Status.objects.create(text='Test content')
        serializer = StatusSerializer(instance=status)
        self.assertEqual(serializer.data['content'], 'Test content')

    def test_read_status_with_acct(self):
        """
        Ensure that serializing a status with a creator generates a correctly nested
        Account object.
        """
        person = Person.objects.create(username='test_user', domain='test_domain')
        status = Status.objects.create(text='Another toot', creator=person)
        serializer = StatusSerializer(instance=status)
        nested_account = serializer.data['account']
        self.assertEqual(nested_account['acct'], 'test_user@test_domain')

    def test_write_status(self):
        """
        Try to create a Status via serializer.
        """
        data = {
            'content': 'this is a foreign post',
            'url': 'https://instance.social/12345',
            'uri': 'https://instance.social/12345', #?
            'account': {
                'acct': 'user@instance.social',
                'bot': False,
                'display_name': 'foreign user'
            }
        }
        serializer = StatusSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        status = serializer.save()
        self.assertEqual(status.text, 'this is a foreign post')
        self.assertEqual(status.creator.username, 'user')
        self.assertEqual(status.creator.domain, 'instance.social')

    def test_write_multiple_status_same_user(self):
        """
        If we get multiple posts by the same user, we should still only end up with a single
        Person object for that user.
        """
        data = {
            'content': 'this is a foreign post',
            'url': 'https://instance.social/12345',
            'uri': 'https://instance.social/12345', #?
            'account': {
                'acct': 'user@instance.social',
                'bot': False,
                'display_name': 'foreign user'
            }
        }
        serializer = StatusSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        first_status = serializer.save()

        data['content'] = 'this is post #2'
        data['url'] = 'https://instance.social/12346'
        data['uri'] = 'https://instance.social/12346'

        serializer = StatusSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        second_status = serializer.save()

        # We should have two different statuses, but with the same creator object
        self.assertNotEqual(first_status, second_status)
        self.assertEqual(first_status.creator, second_status.creator)

    def test_pinned_field(self):
        person = Person.objects.create(username='test_user', domain='test_domain')
        status = Status.objects.create(text='Another toot', creator=person)

        serializer = StatusSerializer(instance=status)
        self.assertFalse(serializer.data['pinned'])

        status.create_pin()
        serializer = StatusSerializer(instance=status)
        self.assertTrue(serializer.data['pinned'])

    def test_reblog_count_and_flag(self):
        """
        Test the 'reblogged' flag which should be true if request.user.person reblogged the status.
        Also the reblogs_count field.
        """
        person = Person.objects.create(username='test_user', domain='test_domain')
        status = Status.objects.create(text='Another toot', creator=person)

        person_b = Person.objects.create(username='another_user', domain='another_domain')
        Status.objects.create(reblog=status, creator=person_b)

        request = Mock()
        request.user.person = person_b

        serializer = StatusSerializer(instance=status, context={'request': request})
        self.assertEqual(serializer.data['reblogs_count'], 1)
        self.assertEqual(serializer.data['reblogged'], True)

    def test_favorite_count_and_flag(self):
        """
        Test the 'favourite' flag which should be true if request.user.person faved the status.
        Also the favourites_count field.
        """
        person = Person.objects.create(username='test_user', domain='test_domain')
        status = Status.objects.create(text='Another toot', creator=person)

        person_b = Person.objects.create(username='another_user', domain='another_domain')
        Favorite.objects.create(creator=person_b, status=status)

        request = Mock()
        request.user.person = person_b

        serializer = StatusSerializer(instance=status, context={'request': request})
        self.assertEqual(serializer.data['favourites_count'], 1)
        self.assertEqual(serializer.data['favourited'], True)

    def test_favorite_and_reblog_null_cases(self):
        """
        Test both favourite and reblogged flags to be false when request.user.person
        did nothing with the status.
        """
        person = Person.objects.create(username='test_user', domain='test_domain')
        status = Status.objects.create(text='Another toot', creator=person)

        # The second person faves and reblogs
        person_b = Person.objects.create(username='another_user', domain='another_domain')
        Favorite.objects.create(creator=person_b, status=status)
        Status.objects.create(reblog=status, creator=person_b)

        # But we pretend the first person is making the request, so they didn't fave or reblog
        request = Mock()
        request.user.person = person

        serializer = StatusSerializer(instance=status, context={'request': request})
        self.assertEqual(serializer.data['favourites_count'], 1)
        self.assertEqual(serializer.data['favourited'], False)
        self.assertEqual(serializer.data['reblogs_count'], 1)
        self.assertEqual(serializer.data['reblogged'], False)
