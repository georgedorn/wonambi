from microblog.models import Status, Favorite, StatusPin
from microblog.tests import MicroblogTestCase
from social.models import PersonFollow, Notification
from django.test.utils import override_settings


class StatusCrudTestCase(MicroblogTestCase):
    status_reblog_url = '/api/v1/statuses/{ID}/reblog'
    status_unreblog_url = '/api/v1/statuses/{ID}/unreblog'
    status_favorite_url = '/api/v1/statuses/{ID}/favourite'
    status_unfavorite_url = '/api/v1/statuses/{ID}/unfavourite'

    def test_post_status_simple(self):
        """
        The bare minimum request to post a toot, associated with the currently-logged-in user.
        """
        data = {
            'status': 'This is a test post.',
        }

        res = self._post_json(self.status_crud_url, data)
        payload = res.json()
        self.assertEqual(payload['account']['acct'], self.person_a.acct)
        self.assertEqual(payload['content'], data['status'])

        # get the most recent status by the user from the DB
        status = Status.objects.filter(creator=self.person_a).last()
        self.assertEqual(status.text, data['status'])

    def test_get_status_simple(self):
        """
        GET /api/v1/statuses/:id should retrieve one status by ID.
        Later permission checks shouldn't affect this test; everybody should be able to retrieve
        their own toots by ID.
        """
        status = Status.objects.create(creator=self.person_a, text="Test post")
        res = self.client.get(self.status_detail_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        toot = res.json()
        self.assertEqual(toot['content'], status.text)
        self.assertEqual(toot['account']['acct'], self.person_a.acct)

    def test_delete_status(self):
        """
        DELETE /api/v1/statuses/:id
        """
        status = Status.objects.create(creator=self.person_a, text="Delete me")
        res = self.client.delete(self.status_detail_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 204)

        self.assertRaises(Status.DoesNotExist, status.refresh_from_db)

    def test_reblog_simple(self):
        """
        POST /api/v1/statuses/:id/reblog
        
        Should create a new Status with the `reblog` field pointing to the old status.
        """
        status = Status.objects.create(creator=self.person_b, text="Original post")

        res = self.client.post(self.status_reblog_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 201)
        payload = res.json()
        
        # outer status should contain logged-in user's account, no text
        self.assertEqual(payload['account']['acct'], self.person_a.acct)
        
        # reblogged status should contain OP's account
        self.assertEqual(payload['reblog']['account']['acct'], self.person_b.acct)
        self.assertEqual(payload['reblog']['content'], status.text)

        # reblogging should create a notification
        note = Notification.objects.get(sender=self.person_a, recipient=self.person_b,
                                        notification_type='reblog')
        self.assertEqual(note.content_object.reblog, status)  # TODO: Confirm this with mastodon UI

    def test_reblog_infinite_chain(self):
        """
        If you try to reblog a reblog using outer-most reblog ID, you should get a new reblog
        containing just the innermost ID.
        """
        Notification.objects.all().delete()  # start with clean slate
        Status.objects.all().delete()
        status = Status.objects.create(creator=self.person_b, text="Original post")
        reblog, _ = Status.objects.get_or_create_reblog(creator=self.person_c, status=status)

        res = self.client.post(self.status_reblog_url.format(ID=reblog.pk))

        self.assertEqual(res.status_code, 201)
        payload = res.json()
        
        # outer status should contain logged-in user's account, no text
        self.assertEqual(payload['account']['acct'], self.person_a.acct)
        
        # reblogged status should contain OP's account
        self.assertEqual(payload['reblog']['account']['acct'], self.person_b.acct)
        self.assertEqual(payload['reblog']['content'], status.text)
        self.assertFalse(payload['reblog']['reblog'])  # None or empty are both okay

        # one OP and two reblogs
        self.assertEqual(Status.objects.count(), 3)

        # And only the OP should be notified, once per reblog
        self.assertEqual(Notification.objects.count(), 2)  # exactly two reblogs happened
        self.assertEqual(Notification.objects.filter(recipient=self.person_b).count(), 2)  # both to OP

    def test_no_repeated_reblogs(self):
        """
        If you try to reblog the same thing multiple times, even using the new reblog's ID, you just get
        the same one back again, unchanged.
        """
        Status.objects.all().delete()
        status = Status.objects.create(creator=self.person_b, text="Original post")

        res = self.client.post(self.status_reblog_url.format(ID=status.pk))

        new_id = res.json()['id']

        res = self.client.post(self.status_reblog_url.format(ID=new_id))
        payload = res.json()
        
        # outer status should contain logged-in user's account, no text
        self.assertEqual(payload['account']['acct'], self.person_a.acct)
        
        # reblogged status should contain OP's account
        self.assertEqual(payload['reblog']['account']['acct'], self.person_b.acct)
        self.assertEqual(payload['reblog']['content'], status.text)
        self.assertFalse(payload['reblog']['reblog'])  # None or empty are both okay

        # Total number of statuses should just be 2
        self.assertEqual(Status.objects.count(), 2)

    def test_reblog_own_private(self):
        """
        You are allowed to "repost to original audience" in the Mastodon UI when a toot is followers-only.
        This is essentially reblogging a private post.
        """
        status = Status.objects.create(creator=self.person_a, text="Original post",
                                       visibility='private')

        res = self.client.post(self.status_reblog_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 201)
        payload = res.json()
        
        # outer status should contain logged-in user's account, no text, and should be private
        self.assertEqual(payload['account']['acct'], self.person_a.acct)
        self.assertEqual(payload['visibility'], 'private')
        
        # reblogged status should contain the original, from yourself, with same visibility
        self.assertEqual(payload['reblog']['account']['acct'], self.person_a.acct)
        self.assertEqual(payload['reblog']['content'], status.text)
        self.assertEqual(payload['reblog']['visibility'], 'private')

        new_status = Status.objects.get(pk=payload['id'])
        self.assertEqual(new_status.visibility, 'private')

    def test_reblog_private_disallowed(self):
        """
        You can't reblog somebody else's private posts.
        """
        # this status belongs to somebody else (we are person_a)
        status = Status.objects.create(creator=self.person_b, text="Private post", visibility='private')

        res = self.client.post(self.status_reblog_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)

    def test_reblog_direct_disallowed(self):
        """
        You can't reblog a DM sent to you.
        """
        # this status belongs to somebody else (we are person_a)
        status = Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_a.acct,
                                       visibility='direct')

        res = self.client.post(self.status_reblog_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)

    def test_reblog_own_direct(self):
        """
        You can't reblog a DM, even if you own it.  Verified this on an existing mastodon instance.
        """
        status = Status.objects.create(creator=self.person_a, text="DM to @%s" % self.person_b.acct,
                                       visibility='direct')

        res = self.client.post(self.status_reblog_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)

    def test_unreblog_by_original_status_ID(self):
        """
        POST /api/v1/statuses/{ORIGINAL_STATUS_ID}/unreblog

        Person B creates a status.  Person A reblogs it, then tries to unreblog it.
        """
        status = Status.objects.create(creator=self.person_b, text="Original post")
        reblog, _ = Status.objects.get_or_create_reblog(status, creator=self.person_a)

        # unreblog using original status ID
        res = self.client.post(self.status_unreblog_url.format(ID=status.pk))

        # don't really care about the response, it's still unclear what the right response is
        self.assertRaises(Status.DoesNotExist, reblog.refresh_from_db)

        # in any case, original status untouched
        status.refresh_from_db()

    def test_unreblog_by_reblog_status_ID(self):
        """
        POST /api/v1/statuses/{REBLOG_STATUS_ID}/unreblog

        Person B creates a status.  Person A reblogs it, then tries to unreblog it.
        """
        status = Status.objects.create(creator=self.person_b, text="Original post")
        reblog, _ = Status.objects.get_or_create_reblog(status, creator=self.person_a)

        # unreblog using original status ID
        res = self.client.post(self.status_unreblog_url.format(ID=reblog.pk))

        # don't really care about the response, it's still unclear what the right response is
        self.assertRaises(Status.DoesNotExist, reblog.refresh_from_db)

        # in any case, original status untouched
        status.refresh_from_db()

    def test_unreblog_not_owner(self):
        """
        Can't unreblog a reblog that isn't yours.
        
        Person A posts a toot, Person B reblogs it, Person A tries to unreblog it.
        """
        status = Status.objects.create(creator=self.person_a, text="Original post")
        reblog, _ = Status.objects.get_or_create_reblog(status, creator=self.person_b)

        # Can't unreblog, neither via original status ID or the new ID.
        res = self.client.post(self.status_unreblog_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 404)  # user doesn't have a reblog of own status
        res = self.client.post(self.status_unreblog_url.format(ID=reblog.pk))
        self.assertEqual(res.status_code, 403)  # user doesn't own that reblog

        # Both fully intact
        status.refresh_from_db()
        reblog.refresh_from_db()

    def test_favorite_simple(self):
        """
        Person B creates a status, Person A favorites it.
        """
        status = Status.objects.create(creator=self.person_b, text="Original post")

        res = self.client.post(self.status_favorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['id'], status.pk)

        self.assertTrue(Favorite.objects.filter(status=status, creator=self.person_a).exists())
        
        # OP should be notified of the fave
        note = Notification.objects.get(recipient=status.creator, sender=self.person_a, notification_type='favourite')
        self.assertEqual(note.content_object, status)

    def test_unfavorite_simple(self):
        """
        Person B's status, Person A's favorite; Person A tries to remove the favorite.
        """
        status = Status.objects.create(creator=self.person_b, text="Original post")
        Favorite.objects.create(status=status, creator=self.person_a)

        res = self.client.post(self.status_unfavorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['id'], status.pk)

        self.assertFalse(Favorite.objects.filter(status=status, creator=self.person_a).exists())

    def test_favorite_private_successful(self):
        """
        Person A follows B, B posts something followers-only, A is allowed to favorite it.
        """
        status = Status.objects.create(creator=self.person_b, text="Followers-only post",
                                       visibility='private')

        res = self.client.post(self.status_favorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['id'], status.pk)

        self.assertTrue(Favorite.objects.filter(status=status, creator=self.person_a).exists())

    def test_favorite_private_unsuccessful(self):
        """
        You can't favorite a toot by somebody you aren't following.
        """
        PersonFollow.objects.all().delete()
        status = Status.objects.create(creator=self.person_b, text="Followers-only post",
                                       visibility='private')

        res = self.client.post(self.status_favorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)

        self.assertEqual(Favorite.objects.count(), 0)

    def test_unfavorite_after_kickout(self):
        """
        You're allowed to un-fave a private toot even after the owner has forced you to stop following them.
        """
        status = Status.objects.create(creator=self.person_b, text="Followers-only post",
                                       visibility='private')
        Status.objects.get_or_create_favorite(status, self.person_a)
        self.assertTrue(Favorite.objects.filter(status=status, creator=self.person_a).exists())

        # unfollow
        PersonFollow.objects.all().delete()

        res = self.client.post(self.status_unfavorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['id'], status.pk)

        self.assertFalse(Favorite.objects.filter(status=status, creator=self.person_a).exists())
        
    def test_favorite_dm(self):
        """
        You're allowed to favorite a DM sent to you.
        """
        status = Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_a.acct,
                                       visibility='direct')
        res = self.client.post(self.status_favorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['id'], status.pk)
        
        # doing so should create a Notification for it
        self.assertTrue(Notification.objects.filter(recipient=self.person_b, sender=self.person_a,
                                                    notification_type='favourite').exists())

        self.assertTrue(Favorite.objects.filter(status=status, creator=self.person_a).exists())

    def test_favorite_dm_not_yours(self):
        """
        You can't favorite a DM that doesn't mention you.
        """
        # B sends a DM to C
        status = Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_c,
                                       visibility='direct')
        # A is not allowed to favorite it
        res = self.client.post(self.status_favorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)
        self.assertEqual(status.favorites.count(), 0)

    def test_unfavorite_dm(self):
        """
        You're allowed to unfavorite a DM sent to you.
        """
        status = Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_a.acct,
                                       visibility='direct')
        Status.objects.get_or_create_favorite(status, self.person_a)
        self.assertTrue(Favorite.objects.filter(status=status, creator=self.person_a).exists())
        
        res = self.client.post(self.status_unfavorite_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['id'], status.pk)

        self.assertFalse(Favorite.objects.filter(status=status, creator=self.person_a).exists())
        
    def test_create_mention(self):
        status = Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_a.acct,
                                       visibility='direct')
        self.assertEqual(status.mentions.count(), 1)
        mention = status.mentions.first()
        self.assertEqual(mention.mentioned, self.person_a)

        # This should create a notification
        note = Notification.objects.filter(sender=self.person_b, recipient=self.person_a, notification_type='mention').last()
        self.assertEqual(note.content_object, status)

    def test_create_mentions_multiple(self):
        text = "DMs to @%s and @%s" % (self.person_a.acct, self.person_c.acct)
        status = Status.objects.create(creator=self.person_b, text=text, visibility='direct')
        self.assertEqual(status.mentions.count(), 2)
        mentioned_accts = [mention.mentioned.acct for mention in status.mentions.all()]

        self.assertTrue(self.person_a.acct in mentioned_accts)
        self.assertTrue(self.person_c.acct in mentioned_accts)

        # A notification for each
        notes = Notification.objects.filter(sender=self.person_b, notification_type='mention')
        for note in notes:
            self.assertEqual(note.content_object, status)
            self.assertTrue(note.recipient in (self.person_a, self.person_c))

    def test_create_status_with_hashtag(self):
        text = "This post is about #stuff"
        status = Status.objects.create(creator=self.person_b, text=text)

        self.assertEqual(status.tags.count(), 1)
        self.assertEqual(status.tags.first().name, 'stuff')

    def test_create_status_multiple_hashtags(self):
        text = 'This post is about #things and #stuff'
        status = Status.objects.create(creator=self.person_b, text=text)

        self.assertEqual(status.tags.count(), 2)
        self.assertTrue(status.tags.filter(name='things').exists())
        self.assertTrue(status.tags.filter(name='stuff').exists())

    def test_status_notifications(self):
        """
        Test getting a collection of notifications about Status objects.
        """
        Notification.objects.all().delete()
        dm = Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_a.acct,
                                   visibility='direct')
        op = Status.objects.create(creator=self.person_a, text="Original post", visibility='public')
        reblog = Status.objects.get_or_create_reblog(op, self.person_c)
        favorite = Status.objects.get_or_create_favorite(op, self.person_c)

        # Create a notification that isn't for Person A
        Status.objects.create(creator=self.person_b, text="DM to @%s" % self.person_c.acct, visibility='direct')

        self.assertEqual(Notification.objects.count(), 4)

        res = self.client.get(self.notifications_url)
        payload = res.json()
        self.assertEqual(len(payload), 3)

        self.assertEqual(payload[0]['type'], 'favourite')
        self.assertEqual(payload[0]['status']['content'], op.text)

        self.assertEqual(payload[1]['type'], 'reblog')   
        self.assertEqual(payload[1]['status']['reblog']['content'], op.text)

        self.assertEqual(payload[2]['type'], 'mention')   
        self.assertEqual(payload[2]['status']['content'], dm.text)
        


class TestStatusPins(MicroblogTestCase):
    status_pin_url = '/api/v1/statuses/{ID}/pin'
    status_unpin_url = '/api/v1/statuses/{ID}/unpin'

    def test_pin_own_status(self):
        """
        User can pin own toot.
        """
        status = Status.objects.create(creator=self.person_a, text='Test Status', visibility='public')
        self.assertFalse(status.is_pinned())

        self.client.post(self.status_pin_url.format(ID=status.pk))
        self.assertTrue(status.is_pinned())

    def test_only_pin_own_status(self):
        """
        Can't pin somebody else's status.
        """
        status = Status.objects.create(creator=self.person_b, text='Status by B', visibility='public')
        self.assertFalse(status.is_pinned())

        res = self.client.post(self.status_pin_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)
        self.assertFalse(status.is_pinned())

    def test_no_repin_status(self):
        """
        Repinning the same status does nothing.
        """
        status = Status.objects.create(creator=self.person_a, text='Test Status', visibility='public')
        status.create_pin()
        self.assertTrue(status.is_pinned())
        self.assertEqual(StatusPin.objects.filter(status=status).count(), 1)

        res = self.client.post(self.status_pin_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)
        
        # but nothing actually changes
        self.assertTrue(status.is_pinned())
        self.assertEqual(StatusPin.objects.filter(status=status).count(), 1)

    def test_no_pin_reblog(self):
        """
        You can't pin a reblog
        """
        status = Status.objects.create(creator=self.person_b, text='Test Status', visibility='public')
        reblog, _ = Status.objects.get_or_create_reblog(status, creator=self.person_a)

        res = self.client.post(self.status_pin_url.format(ID=reblog.pk))
        self.assertEqual(res.status_code, 403)

        self.assertFalse(status.is_pinned())
        self.assertFalse(reblog.is_pinned())

    def test_only_pin_public_unlisted_status(self):
        """
        You can't pin a private or DM.
        """
        for visibility in ('private', 'direct'):
            status = Status.objects.create(creator=self.person_a, text='Test %s Status' % visibility,
                                           visibility=visibility)
            self.assertFalse(status.is_pinned())
        
            res = self.client.post(self.status_pin_url.format(ID=status.pk))
            self.assertEqual(res.status_code, 403)
            self.assertFalse(status.is_pinned())

    def test_unpin_own_status(self):
        status = Status.objects.create(creator=self.person_a, text='Test Status', visibility='public')
        status.create_pin()
        self.assertTrue(status.is_pinned())

        self.client.post(self.status_unpin_url.format(ID=status.pk))
        self.assertFalse(status.is_pinned())

    def test_only_unpin_own_status(self):
        """
        You can't unpin a status belonging to somebody else.
        """
        status = Status.objects.create(creator=self.person_b, text='Status by B', visibility='public')
        status.create_pin()
        self.assertTrue(status.is_pinned())

        res = self.client.post(self.status_unpin_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 403)
        self.assertTrue(status.is_pinned())

    @override_settings(STATUS_PIN_LIMIT=2)
    def test_too_many_pins(self):
        statuses = self._create_toots(number=3, creator=self.person_a)
        statuses[0].create_pin()
        statuses[1].create_pin()

        res = self.client.post(self.status_pin_url.format(ID=statuses[2].pk))
        self.assertEqual(res.status_code, 403)
        self.assertFalse(statuses[2].is_pinned())


class TestAccountStatuses(MicroblogTestCase):
    """
    Tests for GET /api/v1/accounts/:id/statuses with various flags.
    """
    pinned_statuses_url = '/api/v1/accounts/{ID}/statuses?pinned=true'
    account_statuses_url = '/api/v1/accounts/{ID}/statuses'
    exclude_replies_url = '/api/v1/accounts/{ID}/statuses?exclude_replies=true'

    def _create_statuses(self):
        """
        Creates one of each type of status.
        """
        pinned_status = Status.objects.create(creator=self.person_a, text='Test Pinned Status',
                                              visibility='public')
        pinned_status.create_pin()
        self.assertTrue(pinned_status.is_pinned())

        status = Status.objects.create(creator=self.person_a, text='Normal Status', visibility='public')
        
        reply_status = Status.objects.create(creator=self.person_a, text="Reply Status",
                                             in_reply_to=status, visibility='public')
        return (pinned_status, status, reply_status)

    def test_get_pins_for_account(self):
        """
        GET /api/v1/accounts/:id/statuses?pinned=true
        """
        status = Status.objects.create(creator=self.person_a, text='Test Status', visibility='public')
        status.create_pin()
        self.assertTrue(status.is_pinned())

        res = self.client.get(self.pinned_statuses_url.format(ID=self.person_a.pk))
        payload = res.json()
        self.assertEqual(len(payload), 1)

        self.assertEqual(payload[0]['pinned'], True)
        self.assertEqual(payload[0]['content'], status.text)
        self.assertEqual(payload[0]['account']['acct'], self.person_a.acct)

    def test_get_all_statuses_for_account(self):
        """
        GET /api/v1/accounts/:id/statuses
        """
        (pinned_status, status, reply_status) = self._create_statuses()

        res = self.client.get(self.account_statuses_url.format(ID=self.person_a.pk))
        payload = res.json()
        self.assertEqual(len(payload), 3)

        contents = set([item['content'] for item in payload])
        expected_contents = set([pinned_status.text, status.text, reply_status.text])

        self.assertEqual(contents, expected_contents)

    def test_get_statuses_no_replies(self):
        (pinned_status, status, _) = self._create_statuses()

        res = self.client.get(self.exclude_replies_url.format(ID=self.person_a.pk))
        payload = res.json()
        self.assertEqual(len(payload), 2)

        contents = set([item['content'] for item in payload])
        expected_contents = set([pinned_status.text, status.text])

        self.assertEqual(contents, expected_contents)

    def test_account_statuses_pagination(self):
        """
        Account statuses endpoint uses same pagination as timelines.  Ensure this works.
        """
        statuses = self._create_toots(number=20, creator=self.person_a)

        res = self.client.get(self.account_statuses_url.format(ID=self.person_a.pk),
                              {'limit': '10'})

        payload = res.json()
        self.assertEqual(len(payload), 10)
        
        links = res['Link']

        url = res.wsgi_request.get_raw_uri()  # already contains the ?limit=10 part, which appears in header
        expected = '<{URL}&since_id={SINCE_ID}>; rel="prev", <{URL}&max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=statuses[-1].id,  # more recent than last status
            BEFORE_ID=statuses[10].id  # older than the 11th status, as 11 through 20 make 10 statuses.
            )

        self.assertEqual(links, expected)
