"""
Tests of various endpoints related to additional information about Statuses.
"""

from microblog.tests import MicroblogTestCase
from microblog.models import Status, ThreadMute
from social.models import Person


class StatusAccountMetadataTestCase(MicroblogTestCase):
    status_reblogged_by_url = '/api/v1/statuses/{ID}/reblogged_by'
    status_favorited_by_url = '/api/v1/statuses/{ID}/favourited_by'

    def test_reblogged_by_simple(self):
        """
        GET /api/v1/statuses/{ID}/reblogged_by
        Returns a list of accounts that reblogged a status.
        """
        status = Status.objects.create(creator=self.person_b, text="Witty viralbait toot, yo")
        reblog = Status.objects.get_or_create_reblog(status=status, creator=self.person_c)

        res = self.client.get(self.status_reblogged_by_url.format(ID=status.pk))
        accounts = res.json()
        self.assertEqual(len(accounts), 1)

        self.assertEqual(accounts[0]['acct'], self.person_c.acct)

    def test_reblogged_by_pagination(self):
        """
        Test the pagination system for the reblogged_by endpoint.
        
        TODO: The pagination system makes a lot of assumptions about the order of items in payload.
        It's wrong for this purpose, but given that the default limit is 40, it can wait until
        the pagination system is reworked.
        """
        status = Status.objects.create(creator=self.person_b, text="Witty viralbait toot, yo")
        rebloggers = []
        for i in range(8):
            reblogger = Person.objects.create(username=str(i), domain='test.dom')
            Status.objects.get_or_create_reblog(status, reblogger)
            rebloggers.append(reblogger)

        res = self.client.get(self.status_reblogged_by_url.format(ID=status.pk),
                              {'limit': 5})
        payload = res.json()
        self.assertEqual(len(payload), 5)
        accts = [person['acct'] for person in payload]
        self.assertEqual(accts, ['0@test.dom', '1@test.dom', '2@test.dom', '3@test.dom', '4@test.dom'])

        links = res['Link']

        # getting the full URL is non-trivial. The base is probably going to be http://testserver during
        # tests, but the full URL the test client just got is stuffed into TestResponse.wsgi_request
        # anyway.
        url = res.wsgi_request.get_raw_uri()
        # TODO: This is probably backwards from what the UI is expecting.  Fix this with issue #54.
        expected = '<{URL}&since_id={SINCE_ID}>; rel="prev", <{URL}&max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=rebloggers[0].id,
            BEFORE_ID=rebloggers[4].id
            )

        self.assertEqual(links, expected)

    def test_favorited_by_simple(self):
        """
        GET /api/v1/statuses/{ID}/favourited_by
        returns a list of accounts that favorited a status
        """
        status = Status.objects.create(creator=self.person_b, text="Witty viralbait toot, yo")
        favorite = Status.objects.get_or_create_favorite(status=status, creator=self.person_c)

        res = self.client.get(self.status_favorited_by_url.format(ID=status.pk))
        accounts = res.json()
        self.assertEqual(len(accounts), 1)

        self.assertEqual(accounts[0]['acct'], self.person_c.acct)

    def test_favourited_by_pagination(self):
        """
        Test the pagination system for the reblogged_by endpoint.
        
        TODO: The pagination system makes a lot of assumptions about the order of items in payload.
        It's wrong for this purpose, but given that the default limit is 40, it can wait until
        the pagination system is reworked.
        """
        status = Status.objects.create(creator=self.person_b, text="Witty viralbait toot, yo")
        fans = []
        for i in range(8):
            faver = Person.objects.create(username=str(i), domain='test.dom')
            Status.objects.get_or_create_favorite(status, faver)
            fans.append(faver)

        res = self.client.get(self.status_favorited_by_url.format(ID=status.pk),
                              {'limit': 5})
        payload = res.json()
        self.assertEqual(len(payload), 5)
        accts = [person['acct'] for person in payload]
        self.assertEqual(accts, ['0@test.dom', '1@test.dom', '2@test.dom', '3@test.dom', '4@test.dom'])

        links = res['Link']

        # getting the full URL is non-trivial. The base is probably going to be http://testserver during
        # tests, but the full URL the test client just got is stuffed into TestResponse.wsgi_request
        # anyway.
        url = res.wsgi_request.get_raw_uri()
        # TODO: This is probably backwards from what the UI is expecting.  Fix this with issue #54.
        expected = '<{URL}&since_id={SINCE_ID}>; rel="prev", <{URL}&max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=fans[0].id,
            BEFORE_ID=fans[4].id
            )

        self.assertEqual(links, expected)
