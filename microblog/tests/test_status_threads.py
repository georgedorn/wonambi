from microblog.tests import MicroblogTestCase
from microblog.models import Status, ThreadMute


class StatusThreadTestCase(MicroblogTestCase):
    """
    Tests for threading statuses and related endpoints.
    """

    def test_single_post_threading(self):
        """
        Creating a new toot should automatically create a thread for it if it isn't a reply.
        """
        data = {'status': 'Test toot, no reply'}
        res = self._post_json(self.status_crud_url, data)
        status = Status.objects.get(pk=res.json()['id'])
        self.assertIsNotNone(status.thread)

    def test_reply_threading_simple(self):
        """
        Replying to your own toot should set the `in_reply_to_id` field to the parent
        and thread both toots in the same Thread object.
        """
        status = Status.objects.create(creator=self.person_a, text="Test post")

        data = {
            'status': 'This is a test reply.',
            'in_reply_to_id': status.pk
        }

        self._post_json(self.status_crud_url, data)
        reply = status.replies.first()

        self.assertEqual(reply.in_reply_to, status)
        self.assertEqual(reply.thread, status.thread)
        self.assertIsNotNone(status.thread)

    def test_mute_thread_original(self):
        """
        Muting the first status in a thread mutes the whole thread.
        """
        status = Status.objects.create(creator=self.person_b, text="Flirst post")

        res = self.client.post(self.thread_mute_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)

        self.assertTrue(ThreadMute.objects.filter(muter=self.person_a,
                                                  thread=status.thread).exists())

    def test_unmute_thread_original(self):
        status = Status.objects.create(creator=self.person_b, text="Flirst post")

        ThreadMute.objects.create(thread=status.thread, muter=self.person_a)

        res = self.client.post(self.thread_unmute_url.format(ID=status.pk))
        self.assertEqual(res.status_code, 200)

        self.assertFalse(ThreadMute.objects.filter(muter=self.person_a,
                                                   thread=status.thread).exists())

    def test_mute_thread_reply(self):
        """
        Muting a later reply to a status should also mute the thread.
        """
        status = Status.objects.create(creator=self.person_b, text="Flirst post")
        reply = Status.objects.create(in_reply_to=status, creator=self.person_c,
                                      text="Annoying reply that tags @%s" % self.person_a.acct)

        res = self.client.post(self.thread_mute_url.format(ID=reply.pk))
        self.assertEqual(res.status_code, 200)

        self.assertTrue(ThreadMute.objects.filter(muter=self.person_a,
                                                  thread=status.thread).exists())
        self.assertTrue(ThreadMute.objects.filter(muter=self.person_a,
                                                  thread=reply.thread).exists())

    def test_unmute_thread_reply(self):
        """
        Unmuting a later reply to a status should also unmute the thread.
        """
        status = Status.objects.create(creator=self.person_b, text="Flirst post")
        reply = Status.objects.create(in_reply_to=status, creator=self.person_c,
                                      text="Annoying reply that tags @%s" % self.person_a.acct)

        ThreadMute.objects.create(thread=status.thread, muter=self.person_a)
        res = self.client.post(self.thread_unmute_url.format(ID=reply.pk))
        self.assertEqual(res.status_code, 200)

        self.assertFalse(ThreadMute.objects.filter(muter=self.person_a,
                                                  thread=status.thread).exists())
        self.assertFalse(ThreadMute.objects.filter(muter=self.person_a,
                                                  thread=reply.thread).exists())

    def test_get_descendants(self):
        """
        A reply to a status should appear in the descendants part of the /context payload
        for the original status.
        """
        status = Status.objects.create(creator=self.person_b, text="Flirst post")
        reply = Status.objects.create(in_reply_to=status, creator=self.person_c,
                                      text="Reply that tags @%s" % self.person_a.acct)
        reply_to_reply = Status.objects.create(in_reply_to=reply, creator=self.person_b,
                                               text="Reply chain")

        res = self.client.get(self.status_context_url.format(ID=status.pk))
        payload = res.json()
        self.assertEqual(len(payload['descendants']), 2)

        # they should appear oldest to newest
        self.assertEqual(payload['descendants'][0]['content'], reply.text)
        self.assertEqual(payload['descendants'][1]['content'], reply_to_reply.text)

    def test_get_descendants_many(self):
        """
        Create a really long reply chain (more than the limit) and request descendants for the first one.
        """
        status = Status.objects.create(creator=self.person_b, text="Flist poast")
        statuses = [status]
        for i in range(25):
            statuses.append(Status.objects.create(creator=self.person_c, in_reply_to=statuses[-1],
                                                  text=str(i)))

        res = self.client.get(self.status_context_url.format(ID=status.pk))
        payload = res.json()
        self.assertEqual(len(payload['descendants']), 20)

        expected_statuses = statuses[1:21]  # the 20 immediately after the first
        expected_texts = [s.text for s in expected_statuses]

        payload_texts = [p['content'] for p in payload['descendants']]

        self.assertEqual(expected_texts, payload_texts)



    def test_get_ancestors(self):
        """
        The replied-to status should appear in the ancestors of the reply's /context payload.
        """
        status = Status.objects.create(creator=self.person_b, text="Flirst post")
        reply = Status.objects.create(in_reply_to=status, creator=self.person_c,
                                      text="Reply that tags @%s" % self.person_a.acct)
        reply_to_reply = Status.objects.create(in_reply_to=reply, creator=self.person_b,
                                               text="Reply chain")

        res = self.client.get(self.status_context_url.format(ID=reply_to_reply.pk))
        payload = res.json()
        self.assertEqual(len(payload['ancestors']), 2)
        self.assertEqual(payload['ancestors'][0]['content'], status.text)
        self.assertEqual(payload['ancestors'][1]['content'], reply.text)

    def test_get_ancestors_many(self):
        """
        Create a really long reply chain (more than the limit) and request ancestors for the last one.
        """
        status = Status.objects.create(creator=self.person_b, text="Flist poast")
        statuses = [status]
        for i in range(25):
            statuses.append(Status.objects.create(creator=self.person_c, in_reply_to=statuses[-1],
                                                  text=str(i)))
        last_status = statuses[-1]
        
        res = self.client.get(self.status_context_url.format(ID=last_status.pk))
        payload = res.json()
        self.assertEqual(len(payload['ancestors']), 20)

        expected_statuses = statuses[-21:-1]  # the last 20, not counting the one we asked about
        expected_texts = [s.text for s in expected_statuses]

        payload_texts = [p['content'] for p in payload['ancestors']]

        self.assertEqual(expected_texts, payload_texts)

    def test_get_context(self):
        """
        Ancestors and descendants in same payload.
        """
        status = Status.objects.create(creator=self.person_b, text="Flirst post")
        reply = Status.objects.create(in_reply_to=status, creator=self.person_c,
                                      text="Reply that tags @%s" % self.person_a.acct)
        reply_to_reply = Status.objects.create(in_reply_to=reply, creator=self.person_b,
                                               text="Reply chain")

        # get context for the one in the middle
        res = self.client.get(self.status_context_url.format(ID=reply.pk))
        payload = res.json()
        self.assertEqual(len(payload['ancestors']), 1)
        self.assertEqual(payload['ancestors'][0]['content'], status.text)

        self.assertEqual(len(payload['descendants']), 1)
        self.assertEqual(payload['descendants'][0]['content'], reply_to_reply.text)
