"""
Tests of loading the mastodon UI.  No selenium, just ensuring the initial state is
passed correctly.
"""
from social.tests import SocialTestCase
from rest_framework.reverse import reverse


class MastodonHomeTestCase(SocialTestCase):
    
    def test_get_home_page_no_auth(self):
        res = self.client.get("/web/getting-started/")
        self.assertEqual(res.status_code, 302)

    def test_get_home_page_not_logged_in(self):
        self.logout()
        res = self.client.get("/web/getting-started/")
        self.assertEqual(res.status_code, 302)

        expected_redirect = reverse('login')
        self.assertTrue(res['location'].startswith(expected_redirect))

    def test_get_home_page_authed(self):
        pass  # todo: figure out a sane way to do this.  Maybe don't?