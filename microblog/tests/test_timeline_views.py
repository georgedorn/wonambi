from microblog.models import Status
from microblog.tests import MicroblogTestCase
from unittest.case import skip


class TestHomeTimeline(MicroblogTestCase):
    """
    Tests for the /api/v1/timelines/home endpoint:
    - getting the right statuses (follows, DMs, mentions, ?)
    """
        
    def test_get_follows(self):
        """
        Simplest test:  User A follows User B.  User B posts a status; User A should see it.
        """
        status = Status.objects.create(creator=self.person_b,
                                       text='A perfectly normal toot'
                                       )
 
        res = self.client.get(self.home_url)

        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 1)
        toot = payload[0]
        self.assertEqual(toot['content'], status.text)
        toot_account = toot['account']
        self.assertEqual(toot_account['acct'], self.person_b.acct)

    def test_see_own_posts(self):
        """
        The /home/ endpoint should return your own posts as well.
        """
        status = Status.objects.create(creator=self.person_a,
                                       text='A post by test user')

        res = self.client.get(self.home_url)
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 1)
        toot = payload[0]
        self.assertEqual(toot['content'], status.text)
        toot_account = toot['account']
        self.assertEqual(toot_account['acct'], self.person_a.acct)

    @skip("Not yet implemented")
    def test_see_own_posts_oauth(self):
        """
        The /home/ endpoint should return your own posts as well.
        """
        self.logout()
        
        access_token = self._get_oauth_token()  # need to refactor tests to make this available
        status = Status.objects.create(creator=self.person_a,
                                       text='A post by test user')

        res = self.client.get(self.home_url)
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 1)
        toot = payload[0]
        self.assertEqual(toot['content'], status.text)
        toot_account = toot['account']
        self.assertEqual(toot_account['acct'], self.person_a.acct)

    def test_no_DMs(self):
        """
        The /home/ endpoint shows public, private and unlisted toots, but not 'direct' toots (DMs).
        """
        Status.objects.create(creator=self.person_a,
                              text='A DM by test user',
                              visibility='DIRECT')
        
        res = self.client.get(self.home_url)
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 0)


class TestPublicTimeline(MicroblogTestCase):
    """
    Tests for /api/v1/timelines/public.
    """

    def test_public_only(self):
        """
        Create several toots, retrieve the public timeline, only see toots with visibility=public.
        """
        dm = Status.objects.create(creator=self.person_a, visibility='direct',
                                   text="@B@someother.instance hi there")
        unlisted = Status.objects.create(creator=self.person_b, visibility='unlisted',
                                         text="This is an unlisted toot.")
        public_a = Status.objects.create(creator=self.person_a, visibility='public',
                                         text="Public toot by A@test.instance")
        public_b = Status.objects.create(creator=self.person_b, visibility='public',
                                         text="Public toot by B@someother.instance")

        res = self.client.get(self.public_url)

        # we should only see two toots here
        self.assertEqual(res.status_code, 200)
        
        payload = res.json()
        self.assertEqual(len(payload), 2)
        texts = [toot['content'] for toot in payload]

        self.assertTrue(public_a.text in texts)
        self.assertTrue(public_b.text in texts)

        self.assertFalse(dm.text in texts)
        self.assertFalse(unlisted.text in texts)

    def test_public_local_only(self):
        Status.objects.create(creator=self.person_a, visibility='public',
                              text='Public toot by A@test.instance',
                              local=True)
        # TODO: Toots create by users on other instances should automatically set local=False
        Status.objects.create(creator=self.person_b, visibility='public',
                              text='Public toot by B@someother.instance',
                              local=False)
        res = self.client.get(self.public_url, {'local': 'true'})
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.json()), 1)

    def test_public_no_replies(self):
        """
        Replies to other toots are not supposed to appear in the public timeline, local or otherwise.
        """
        first_status = Status.objects.create(creator=self.person_a, visibility='public',
                                             text='Public toot by A@test.instance',
                                             local=True)
        reply = Status.objects.create(creator=self.person_b, visibility='public',
                                      text="Reply to user A by user B",
                                      in_reply_to=first_status,
                                      local=False)

        res = self.client.get(self.public_url)
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.json()), 1)

        self.assertNotEqual(reply.text, res.json()[0]['content'])


class TestDirectTimeline(MicroblogTestCase):
    def test_direct_messages_simple(self):
        """
        DMs to you show up in GET /api/v1/timelines/direct
        """
        status = Status.objects.create(creator=self.person_b, visibility='direct',
                                       text='DM to @%s' % self.person_a.acct)
        res = self.client.get(self.direct_url)
        payload = res.json()
        self.assertEqual(len(payload), 1)
        self.assertEqual(payload[0]['content'], status.text)

        # Mentions field is populated
        mentions = payload[0]['mentions']
        self.assertEqual(len(mentions), 1)
        mention = mentions[0]
        self.assertEqual(mention['acct'], self.person_a.acct)
        self.assertEqual(mention['username'], self.person_a.username)

    def test_direct_messages_replies(self):
        """
        DMs _from_ you also show up in the timeline.
        """
        status = Status.objects.create(creator=self.person_b, visibility='direct',
                                       text='DM to @%s' % self.person_a.acct)
        reply = Status.objects.create(creator=self.person_a, visibility='direct',
                                      text="@%s hi there" % self.person_b.acct,
                                      in_reply_to=status)
        res = self.client.get(self.direct_url)

        payload = res.json()
        self.assertEqual(len(payload), 2)

        # reverse chronological order - older toots show up later in the payload
        self.assertEqual(payload[0]['content'], reply.text)
        self.assertEqual(payload[1]['content'], status.text)
        self.assertEqual(payload[1]['replies_count'], 1)


class TestTagTimeline(MicroblogTestCase):
    """
    Tests for /api/v1/timelines/tag/:hashtag
    """
    tag_timeline_url = "/api/v1/timelines/tag/{HASHTAG}"
    
    def test_get_timeline(self):
        """
        Filter a timeline by status.
        """
        Status.objects.create(text="Some other post without a tag",
                              creator=self.person_b)
        Status.objects.create(text="Some other post without a tag",
                              creator=self.person_a)
        for i in range(20):
            Status.objects.create(text="Post no. %s about #stuff" % i,
                                  creator=self.person_b)
    
        res = self.client.get(self.tag_timeline_url.format(HASHTAG='stuff'))
        payload = res.json()
        self.assertEqual(len(payload), 20)

        # Order matters - it should be newest first, oldest last
        self.assertEqual(payload[-1]['content'], "Post no. 0 about #stuff")
        self.assertEqual(payload[0]['content'], "Post no. 19 about #stuff")
        self.assertEqual(payload[0]['tags'][0]['name'], 'stuff')

    def test_get_timeline_no_entries(self):
        """
        Test getting a tag that doesn't exist.
        """
        res = self.client.get(self.tag_timeline_url.format(HASHTAG='nada'))
        self.assertEqual(res.status_code, 404)

    def test_get_timeline_hashtag_case(self):
        """
        Case doesn't matter when requesting statuses matching a hashtag.
        """
        Status.objects.create(text="A post about #thingsandstuff",
                              creator=self.person_b)
        Status.objects.create(text="A different post about #ThingsAndStuff",
                              creator=self.person_a)

        res = self.client.get(self.tag_timeline_url.format(HASHTAG='thingsandstuff'))
        payload = res.json()
        self.assertEqual(len(payload), 2)

        res = self.client.get(self.tag_timeline_url.format(HASHTAG='ThingsAndStuff'))
        payload = res.json()
        self.assertEqual(len(payload), 2)


class TestPagination(MicroblogTestCase):
    """
    Tests of pagination of timelines.
    """

    def test_max_id(self):
        """
        Specifying the max_id parameter should retrieve older entries.
        Note the off-by-one error built into the API spec:
        max_id     Return results older than ID     Optional
                                  ^^^^^
        """
        first_status = Status.objects.create(creator=self.person_a,
                                             text='First toot')
        second_status = Status.objects.create(creator=self.person_a,
                                              text='Second toot')
        before_id = second_status.id
        res = self.client.get(self.home_url, {'max_id': str(before_id)})

        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 1)
        toot = payload[0]
        self.assertEqual(toot['content'], first_status.text)

    def test_since_id(self):
        """
        Specifying since_id should retrieve newer entries.
        """
        first_status = Status.objects.create(creator=self.person_a,
                                             text='First toot')
        second_status = Status.objects.create(creator=self.person_a,
                                              text='Second toot')
        
        since_id = first_status.id
        res = self.client.get(self.home_url, {'since_id': str(since_id)})

        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 1)
        toot = payload[0]
        self.assertEqual(toot['content'], second_status.text)

    def test_prev_next_links(self):
        """
        The response header should contain a link to follow to get "previous" toots.
        This is deeply confusing as the concept of 'previous' and 'next' are reversed when dealing
        with a reverse-chronological timeline.
        """
        statuses = self._create_toots(10)
        res = self.client.get(self.home_url)
        links = res['Link']

        # getting the full URL is non-trivial. The base is probably going to be http://testserver during
        # tests, but the full URL the test client just got is stuffed into TestResponse.wsgi_request
        # anyway.
        url = res.wsgi_request.get_raw_uri()
        expected = '<{URL}?since_id={SINCE_ID}>; rel="prev", <{URL}?max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=statuses[-1].id,
            BEFORE_ID=statuses[0].id
            )

        self.assertEqual(links, expected)
        
    def test_prev_next_links_with_limit(self):
        """
        The response header should contain a link to follow to get "previous" toots.
        This is deeply confusing as the concept of 'previous' and 'next' are reversed when dealing
        with a reverse-chronological timeline.
        """
        statuses = self._create_toots(20)
        res = self.client.get(self.home_url, {'limit': '10'})

        payload = res.json()
        self.assertEqual(len(payload), 10)
        
        links = res['Link']

        url = res.wsgi_request.get_raw_uri()  # already contains the ?limit=10 part, which appears in header
        expected = '<{URL}&since_id={SINCE_ID}>; rel="prev", <{URL}&max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=statuses[-1].id,  # more recent than last status
            BEFORE_ID=statuses[10].id  # older than the 11th status, as 11 through 20 make 10 statuses.
            )

        self.assertEqual(links, expected)
        
        