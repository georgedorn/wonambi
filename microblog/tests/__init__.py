from social.tests import SocialTestCase
from microblog.models import Status


class MicroblogTestCase(SocialTestCase):
    """
    Useful base class for tests involving microblog APIs.
    """
    # Normally we'd use reverse() to avoid hard-coding api endpoints, but we're specifically
    # trying to clone an existing API, so this helps with testing for parity.
    home_url = '/api/v1/timelines/home'
    public_url = '/api/v1/timelines/public'
    direct_url = '/api/v1/timelines/direct'

    # used by several child testcases
    status_crud_url = '/api/v1/statuses'
    status_detail_url = '/api/v1/statuses/{ID}'

    thread_mute_url = '/api/v1/statuses/{ID}/mute'
    thread_unmute_url = '/api/v1/statuses/{ID}/unmute'

    status_context_url = '/api/v1/statuses/{ID}/context'

    def _create_toots(self, number, creator=None, **kwargs):
        """
        Helper method to create a bunch of toots at once.
        Arbitrary kwargs are passed to Status.objects.create()
        """
        if creator is None:
            creator = self.person_a
        toots = []
        for i in range(number):
            # could do bulk_create, but only postgres is able to return PKs
            status = Status.objects.create(creator=creator,
                                           text="Toot #%s" % i,
                                           **kwargs)
            toots.append(status)
        return toots
