from django.conf import settings
from social.serializers import PersonSerializer

from django.shortcuts import render, redirect
from oauth.utils import create_access_token
from rest_framework.reverse import reverse
from django.contrib.auth.decorators import login_required


@login_required
def mastodon_home(request):
    """
    View to set up the single-page-app (aka Mastodon UI) and pass off auth credentials.
    Currently redirects to the oauth authorization view if there's no available Oauth Grant,
    and redirects to the login view if there's no user at all.
    
    The oauth stuff is likely fragile.
    
    @TODO:  Try using this without OAuth; it may work with Django's cookies alone.
    @TODO:  If it won't work without OAuth, oauth-toolkit likely redirects here with ?code=bearer_token
    """
    person = request.user.person
    person_serializer = PersonSerializer(instance=person)
    
    access_token = create_access_token(request)
    if access_token is None:
        #TODO:  Maybe ignore this?  Django session auth might work just fine for this.
        return redirect(reverse('oauth2_provider:authorize'))
    
    initial_state = {
        "accounts": {
            str(person.pk): person_serializer.data
        },
        "char_limit": settings.STATUS_MAX_LENGTH,
        "compose": {
            "me": person.pk,
            "default_privacy": 'public',  # TODO: configurable option for users
            "default_sensitive": False,  # TODO: configurable option for users
        },
        "custom_emojis": [],  # TODO
        "media_attachments": {"accept_content_types": [".jpg", ".gif"]},  # TODO: update when uploads implemented
        "meta": {
            "access_token": access_token,
            "admin": str(int(request.user.is_staff)),  # ugh
            "auto_play_gif": False,
            "boost_modal": False,
            "delete_modal": False,
            "display_sensitive_media": False,
            "domain": settings.DOMAIN,
            "locale": "en",
            "max_toot_chars": settings.STATUS_MAX_LENGTH,
            "me": person.pk,
            "reduce_motion": False,
            "streaming_api_base_url": "",
            "unfollow_modal": False
        },
        "push_subscriptions": None,
        "rights": {
            "admin": request.user.is_staff,
            "delete_others_notice": request.user.is_staff,
        },
        "settings": []  # TODO: Make a PersonSettingsSerializer for this?     
        
    }
    
    return render(request, 'mastodon_home.html', {'initial_state': initial_state,
                                                  'settings': settings})
