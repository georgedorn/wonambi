"""
Models pertaining to microblogging.
Mastodon refers to these as Toots in the UI and 'statuses' in the db.
"""
import re

from django.db import models

from wonambi.utils.models import TimestampedModel
from social.models import Person
from tagging.models import Tag


class StatusQuerySet(models.QuerySet):
    """
    Implements common queries or filters on Status objects.
    """
    def without_replies(self):
        """
        Filter out statuses that are replies to other statuses.
        """
        return self.filter(in_reply_to=None)

    def local_only(self):
        """
        Filter out statuses from other instances.
        """
        return self.filter(local=True)


class StatusManager(models.Manager):
    
    def get_or_create_reblog(self, status, creator):
        # Don't allow reblog chains; Mastodon specifically prevents this.
        # If you see a reblog in your timeline and reblog it, you reblog the _original_ toot.
        while status.reblog is not None:
            status = status.reblog

        defaults = {
            'content_warning': status.content_warning,
            'visibility': status.visibility,
            'is_sensitive': status.is_sensitive
            }
        return self.get_or_create(
            reblog=status,
            creator=creator,
            defaults=defaults
            )

    def get_or_create_favorite(self, status, creator):
        """
        Creates a Favorite object for a status, or gets an existing one if the status has already
        been favorited.
        """
        return Favorite.objects.get_or_create(
            creator=creator,
            status=status
        )


VISIBILITY_CHOICES = [
    ('public', 'PUBLIC'),
    ('direct', 'DIRECT'),
    ('unlisted', 'UNLISTED'),
    ('private', 'PRIVATE')
    ]


class Status(TimestampedModel):
    """
    A post.
    """
    creator = models.ForeignKey('social.Person', null=True, on_delete=models.SET_NULL, related_name='posts')
    uri = models.CharField(max_length=64, blank=True)  # denormalized URI?  TODO: validator
    url = models.URLField(help_text='URL to original content of post.')
    text = models.TextField(help_text='Content of post.')  # Don't enforce length here.
    in_reply_to = models.ForeignKey('microblog.Status', null=True, on_delete=models.SET_NULL, related_name='replies',
                                    help_text='Status this status is a reply to.')
    in_reply_to_account = models.ForeignKey('social.Person', null=True, on_delete=models.SET_NULL, related_name='+',
                                            help_text='Account that owns the Status this status is a reply to.')
    reblog = models.ForeignKey('microblog.Status', null=True, on_delete=models.SET_NULL, related_name='reblogs',
                                     help_text='Status this status is a reblog of.')

    content_warning = models.CharField(max_length=256, help_text='Spoiler text / content warning for post.')
    is_sensitive = models.BooleanField(default=False, help_text="Whether status is marked as sensitive, requiring additional click to show content on some clients.")

    visibility = models.CharField(max_length=10, help_text='Visibility level of post; public, private, unlisted, direct',
                                  choices=VISIBILITY_CHOICES, default='public')
    language = models.CharField(max_length=32, help_text="Language (BCP47) of post")
    local = models.BooleanField(default=False, help_text="Whether status is local to instance; TODO: maybe not needed, if this can be deduced from uri/url?")

    thread = models.ForeignKey('microblog.Thread', null=True, on_delete=models.CASCADE, related_name='conversation')

    class Meta:
        ordering = ('-id',)

    objects = StatusManager.from_queryset(StatusQuerySet)()

    def save(self, *args, **kwargs):
        """
        On-save activities that only impact local db; federation should be implemented only in signals.
        """
        self._handle_threading()
        super().save(*args, **kwargs)
        self._handle_mentions()
        self._handle_tags()

    def _handle_threading(self):
        """
        Associate this Status with the Thread it is replying to, or a new one if it isn't a reply.
        """
        # associate with a thread object to allow querying for replies without recursion
        if not self.thread:
            if not self.in_reply_to:
                self.thread = Thread.objects.create()
            else:
                self.thread = self.in_reply_to.thread

    # This regex modified from Pleroma's.
    # 1: convert to python
    # 2: allow underscores in hostnames (that's valid)
    MENTION_RE = \
        r"@[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]*@?[[a-zA-Z0-9](?:[a-zA-Z0-9-_]{0,61}[a-zA-Z0-9_])?(?:\.[a-zA-Z0-9_](?:[a-zA-Z0-9-_]{0,61}[a-zA-Z0-9_])?)*"

    # This regex from Mastodon, but uses look-behind and python chokes on it
    #    USERNAME_RE = r'[a-z0-9_]+([a-z0-9_\.]+[a-z0-9_]+)?'
    #    MENTION_RE  = r'(?<=^|[^\/[:word:]])@(({USERNAME_RE})(?:@[a-z0-9\.\-]+[a-z0-9]+)?)'.format(USERNAME_RE=USERNAME_RE)

    # This regex from the 'federation' library
    #    MENTION_RE = r'@{[^;]+; [\w.-]+@[^}]+}'
    def _handle_mentions(self):
        """
        Parse the text of this Status and create Mention objects as needed.
        """
        mentions = re.findall(Status.MENTION_RE, self.text, re.UNICODE | re.IGNORECASE)
        for mention in mentions:
            acct = mention.lstrip('@')
            parts = acct.split('@')
            if len(parts) != 2:
                continue  # random errant @ sign?
            username, domain = acct.split('@')
            # TODO:  Consider throwing away mentions to Persons not already known
            person, _ = Person.objects.get_or_create(username=username, domain=domain)
            Mention.objects.get_or_create(status=self, mentioned=person)

    # Hashtag regex from Mastodon, converted to Python
    # HASHTAG_NAME_RE = r'[[:word:]_]*[[:alpha:]_·][[:word:]_]*'
    # HASHTAG_RE = r'(?:^|[^/)\w])#(#{HASHTAG_NAME_RE})'.format(HASHTAG_NAME_RE=HASHTAG_NAME_RE)

    # Hashtag regex from Pleroma, converted to Python
    HASHTAG_RE = r'((?<=[^&])|\A)(\#)(\w+)'

    def _handle_tags(self):
        """
        Parse the text of this Status to extract hashtags; create or associate with Tag objects
        accordingly.
        
        Note that we are permitting different capitalizations of tags to exist as different Tag objects;
        API endpoints will need to specify __iexact to get all case versions of a tag.
        """
        hashtag_matches = re.findall(Status.HASHTAG_RE, self.text, re.UNICODE)
        # re.findall returns a list of tuples of groups; we only care about the last in each tuple
        hashtags = [h[-1] for h in hashtag_matches if h[-1] not in ('', '#')]
        for hashtag in hashtags:
            Tag.objects.add_tag(self, hashtag)

    def mentions_person(self, person):
        """
        Helper method; returns true if this status mentions a person object. 
        """
        return self.mentions.filter(mentioned=person).exists()

    def create_pin(self):
        """
        Creates a StatusPin for a toot.  Doesn't take a Person, as the creator should be the only one
        pinning it.  Can't pin a toot more than once.
        """
        StatusPin.objects.get_or_create(status=self, person=self.creator)

    def delete_pin(self):
        """
        Deletes a pin for this toot.
        """
        StatusPin.objects.filter(status=self).delete()

    def is_pinned(self):
        """
        Returns true if there's a pin for this status.
        """
        return StatusPin.objects.filter(status=self, person=self.creator).exists()

    @property
    def ancestors(self, limit=20):
        """
        Returns up to `limit` statuses in this thread that occur before this one.
        """
        statuses = Status.objects.filter(thread=self.thread, created_at__lte=self.created_at) \
                    .exclude(pk=self.pk).order_by('-created_at')  # newest first
        return statuses[0:limit:-1]  # but need to reverse that so that oldest is first

    @property
    def descendants(self, limit=20):
        """
        Returns up to `limit` statuses in this thread that occur after this one.
        """
        # Oldest first, so that a direct reply to this status is (more or less) first in the
        statuses = Status.objects.filter(thread=self.thread, created_at__gte=self.created_at) \
                    .exclude(pk=self.pk).order_by('created_at')  # oldest first
        return statuses[0:limit]


# Status objects can have tags
from tagging.registry import register
register(Status)


class Thread(TimestampedModel):
    """
    Collections of Statuses are Threads (Conversations).
    """
    uri = models.CharField(max_length=64, blank=True)  # denormalization to first status in thread?


class ThreadMute(TimestampedModel):
    muter = models.ForeignKey('social.Person', help_text='Actor doing the muting.',
                              on_delete=models.CASCADE)
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)


class Favorite(TimestampedModel):
    creator = models.ForeignKey('social.Person', null=True, on_delete=models.SET_NULL,
                                related_name='favoriters')
    status = models.ForeignKey('microblog.Status', null=True, on_delete=models.SET_NULL,
                               related_name='favorites')


class Mention(TimestampedModel):
    """
    Relates statuses directly to the people they mention.  In the case of Direct Messages,
    that also indicates recipients and attendant permissions.
    """
    mentioned = models.ForeignKey('social.Person', null=False, on_delete=models.CASCADE,
                                  related_name='mentions')
    status = models.ForeignKey('microblog.Status', null=False, on_delete=models.CASCADE,
                               related_name='mentions')


class StatusPin(TimestampedModel):
    """
    Relates statuses to an account that chose to pin a given status.
    Accounts should only be able to pin their own statuses.
    
    """
    # no need for related_name; a status should only be pinned by creator
    status = models.ForeignKey('microblog.Status', null=False, on_delete=models.CASCADE,
                               related_name='+')

    #Get all of a person's pins with:  person.status_pins.all()
    person = models.ForeignKey('social.Person', null=True, on_delete=models.CASCADE,
                               related_name='status_pins')
