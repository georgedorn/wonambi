# Generated by Django 2.1.1 on 2018-10-02 21:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('uri', models.CharField(blank=True, max_length=64)),
                ('url', models.URLField(help_text='URL to original content of post.')),
                ('text', models.TextField(help_text='Content of post.')),
                ('content_warning', models.CharField(help_text='Spoiler text / content warning for post.', max_length=256)),
                ('is_sensitive', models.BooleanField(default=False, help_text='Whether status is marked as sensitive, requiring additional click to show content on some clients.')),
                ('visibility', models.CharField(choices=[('public', 'PUBLIC'), ('direct', 'DIRECT'), ('unlisted', 'UNLISTED'), ('private', 'PRIVATE')], help_text='Visibility level of post; public, private, unlisted, direct', max_length=10)),
                ('language', models.CharField(help_text='Language (BCP47) of post', max_length=32)),
                ('local', models.BooleanField(default=False, help_text='Whether status is local to instance; TODO: maybe not needed, if this can be deduced from uri/url?')),
            ],
            options={
                'abstract': False,
                'ordering': ('-id',)
            },
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('uri', models.CharField(blank=True, max_length=64)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ThreadMute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
