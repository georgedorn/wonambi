from rest_framework import serializers

from social.serializers import PersonSerializer
from .models import Status
from rest_framework_recursive.fields import RecursiveField
from tagging.models import Tag
from microblog.models import Mention


class StatusTagSerializer(serializers.ModelSerializer):
    """
    Serialize a django-tagging Tag relationship to Status objects.
    """
    name = serializers.CharField()
    url = serializers.SerializerMethodField()
    history = serializers.SerializerMethodField()
    
    class Meta:
        model = Tag
        fields = ('name', 'url', 'history')

    def get_url(self, obj):
        # TODO: Refactor this to use reverse() when ready
        return '/api/v1/timelines/tag/{HASHTAG}'.format(HASHTAG=obj.name)

    def get_history(self, obj):
        return []


class MentionSerializer(serializers.ModelSerializer):
    url = serializers.CharField(source='mentioned.url')
    username = serializers.CharField(source='mentioned.username')
    id = serializers.CharField(source='mentioned.pk')
    acct = serializers.CharField(source='mentioned.acct')

    class Meta:
        model = Mention
        fields = ('url', 'username', 'acct', 'id')

class StatusSerializer(serializers.ModelSerializer):
    account = PersonSerializer(read_only=False, many=False, source='creator')
    content = serializers.CharField(source='text')
    sensitive = serializers.SerializerMethodField()
    spoiler_text = serializers.CharField(source='content_warning', required=False)
    visibility = serializers.CharField(required=False)
    language = serializers.CharField(required=False)

    url = serializers.URLField(required=False)
    tags = StatusTagSerializer(many=True, read_only=False, required=False)
    pinned = serializers.SerializerMethodField()  # get_pinned(), below
    reblog = RecursiveField(allow_null=True, required=False, read_only=True)
    in_reply_to_id = serializers.PrimaryKeyRelatedField(source='in_reply_to',
                                                        queryset=Status.objects.all(),
                                                        required=False)

    mentions = MentionSerializer(many=True, required=False, read_only=True)

    # Stats
    replies_count = serializers.IntegerField(source='replies.count', read_only=True)
    reblogs_count = serializers.IntegerField(source='reblogs.count', read_only=True)
    favourites_count = serializers.IntegerField(source='favorites.count', read_only=True)

    # Booleans
    reblogged = serializers.SerializerMethodField()
    favourited = serializers.SerializerMethodField()

    # filler fields
    emojis = serializers.SerializerMethodField(method_name='get_empty_list')
    media_attachments = serializers.SerializerMethodField(method_name='get_empty_list')
    application = serializers.SerializerMethodField(method_name='get_null')
    muted = serializers.SerializerMethodField(method_name='get_false')
    
    class Meta():
        model = Status
        fields = ('id', 'url', 'uri', 'account', 'in_reply_to_id', 'in_reply_to_account_id',
                  'reblog', 'content', 'emojis', 'replies_count', 'reblogs_count',
                  'favourites_count', 'reblogged', 'favourited', 'muted', 'sensitive', 'spoiler_text',
                  'visibility', 'media_attachments', 'mentions', 'tags', 'application', 'language',
                  'pinned', 'created_at')

    def get_reblogged(self, obj):
        """
        Returns true if the current person reblogged this status.
        """
        if 'request' not in self.context:
            return False  # In some cases, nobody is logged in, so the user obviously hasn't reblogged
        person = self.context['request'].user.person
        return bool(person and person.pk in obj.reblogs.values_list('creator', flat=True))
    
    def get_favourited(self, obj):
        """
        Returns true if the current person favorited this status.
        """
        if 'request' not in self.context:
            return False  # In some cases, nobody is logged in, so the user obviously hasn't favorited
        person = self.context['request'].user.person
        return bool(person and person.pk in obj.favorites.values_list('creator', flat=True))

    def create(self, validated_data):
        """
        DRF doesn't automatically handle nested data during .create() because it's impossible for it
        to solve all design decisions automatically.
        
        In our case, the same `account` may appear multiple times, even in the same Status, and therefore
        we only want to create one and update it as new data comes in.
        """
        # get_or_create the `creator` object from the top-level status.
        account_serializer = PersonSerializer(data=validated_data['creator'])
        account_serializer.is_valid(raise_exception=True)
        validated_data['creator'] = account_serializer.update_or_create()

        status = super().create(validated_data)

        return status

    def get_sensitive(self, obj):
        return bool(obj.content_warning)

    def get_pinned(self, obj):
        return obj.is_pinned()

    # Filler fields below
    def get_empty_list(self, obj):
        return []

    def get_zero(self, obj):
        """
        Dummy method to get the API shape right.
        """
        return 0

    def get_false(self, obj):
        """
        Dummy method to populate booleans.
        """
        return False

    def get_null(self, obj):
        return None
