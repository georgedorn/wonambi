from wonambi.utils.router import OptionalSlashRouter
from microblog.api import StatusViewSet, TimelineViewSet, AccountStatusView
from django.conf.urls import url

router = OptionalSlashRouter()

router.register(r'timelines', TimelineViewSet)
router.register(r'statuses', StatusViewSet)

urlpatterns = router.urls


# /api/v1/accounts/:id/statuses/ retrieves statuses, so it belongs here in microblog.urls, not social.urls
urlpatterns += [
    url(r'^accounts/(?P<pk>[^/.]+)/statuses/?$', AccountStatusView.as_view()),  # ??? not routed to?
    
]