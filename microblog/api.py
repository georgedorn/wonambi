from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.conf import settings

from rest_framework import viewsets, permissions, generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK, HTTP_403_FORBIDDEN

from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication

from tagging.models import Tag, TaggedItem

from social.models import Person
from social.serializers import PersonSerializer, NotificationSerializer

from wonambi.utils.pagination import LiveResponseHeaderPagination
from wonambi.utils.api import get_boolean_query_param, PermissionsPerMethodMixin


from .models import Status, Favorite, StatusPin, ThreadMute
from .serializers import StatusSerializer
from django.contrib.auth.models import AnonymousUser


class TimelineViewSet(PermissionsPerMethodMixin, viewsets.ModelViewSet):
    """
    API endpoints for /api/v1/timelines/*
    Status CRUD happens in StatusViewSet.
    """
    authentication_classes = (OAuth2Authentication, SessionAuthentication)
    serializer_class = StatusSerializer
    queryset = Status.objects.all().prefetch_related('mentions')
    pagination_class = LiveResponseHeaderPagination

    def list(self, request):
        """
        TODO:  Somehow, this should handle:
        /api/v1/timelines/list/:list_id
        but since list is a keyword for DRF...
        """
        print('Tried to access StatusViewSet.list()')
        raise NotImplementedError("Not ready yet.")

    @action(detail=False)
    @permission_classes((IsAuthenticated,),)
    def home(self, request):
        """
        Endpoint to retrieve home timeline for a user.
        """
        person = request.user.person
        followed_users = person.following.all()  # gets a queryset of IDs of Person objects
        
        status_filter = Q(creator__in=followed_users) | Q(creator=person)
        
        statuses = self.get_queryset().filter(status_filter)

        statuses = self._apply_filters(statuses)  # common filters like local, only_media, etc

        home_timeline_visibility = ('public', 'private', 'unlisted')
        home_statuses = statuses.filter(visibility__in=home_timeline_visibility).order_by('-id')

        return LiveResponseHeaderPagination._generate_paginated_response(request, home_statuses, StatusSerializer)

    @action(detail=False)
    def public(self, request):
        """
        Endpoint to retrieve the instance's "public" timeline.
        Behaves roughly the same as /home/ but only returns 'public' toots and doesn't care
        about followers.
        """
        public_statuses = self.get_queryset().filter(visibility='public').without_replies()

        public_statuses = self._apply_filters(public_statuses)
        
        return LiveResponseHeaderPagination._generate_paginated_response(request, public_statuses,
                                                                         StatusSerializer)

    @action(detail=False)
    @permission_classes((IsAuthenticated,),)
    def direct(self, request):
        """
        Endpoint to retrieve user's DMs.
        Relies on Mention objects to determine what to show.
        """
        person = request.user.person

        dm_filter = Q(creator=person) | Q(mentions__mentioned=person)
        
        dm_statuses = Status.objects.filter(dm_filter).filter(visibility='direct')

        return LiveResponseHeaderPagination._generate_paginated_response(request, dm_statuses,
                                                                         StatusSerializer)

    @action(detail=False, url_path='tag/(?P<hashtag>[^/]+)')
    def tag(self, request, hashtag):
        """
        Retrieve status by tag.
        @TODO:  check filters against permissions; this should only retrieve statuses the user
                is allowed to see.
        """
        tag = get_object_or_404(Tag, name=hashtag.lower())
        statuses = TaggedItem.objects.get_by_model(self.get_queryset(), tag)
        return LiveResponseHeaderPagination._generate_paginated_response(request, statuses,
                                                                         StatusSerializer)

    def _apply_filters(self, queryset):
        """
        Several filters can be applied to any endpoint (at least in theory):
        - local = local toots only
        """
        if get_boolean_query_param(self.request, 'local'):
            queryset = queryset.local_only()
        
        return queryset


class IsOwnerOrCanViewStatus(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it, and control
    access to view a Status based on its `visibility` and the relationship between the user
    and owner.
    
    It is generally unnecessary to combine this with IsAuthenticated, as Anonymous users are
    accounted for here.
    """

    def has_object_permission(self, request, view, obj):
        """
        Determines if the request's user is allowed to take a given action on a given Status.
        """
        user = request.user
        # admins can see or do anything
        if user.is_staff or user.is_superuser:
            return True

        # anonymous users can only view public posts
        if isinstance(request.user, AnonymousUser):
            if obj.visibility in ('public', 'unlisted') and request.method in permissions.SAFE_METHODS:
                return True
            return False  # Anon users can't do anything else

        # users can do anything with their own statuses
        if obj.creator == user.person:  # TODO: fix if implementing multiple Person per User
            return True

        # simple CRUD
        if request.method in permissions.SAFE_METHODS:
            if obj.visibility in ('public', 'unlisted'):
                # TODO: if author has blocked user, they can't see it.
                return True

            # otherwise, things get complicated
            if obj.visibility == 'direct':
                # only those mentioned in a DM can view it
                return obj.mentions.filter(mentioned=user.person).exists()

            if obj.visibility == 'private':
                # only those mentioned or following the author can view it
                return (
                    obj.mentions_person(user.person) or
                    user.person.is_following(obj.creator)
                )
            
        # For non-CRUD endpoints decorated with @action, the name of the method is provided in the view
        # @TODO: Separate this into decorators and don't do the view-level logic here.
        action = view.action

        # This can just be IsAllowedToSee
        if obj.visibility in ('public', 'unlisted'):
            if action in ('reblog', 'unreblog', 'favourite', 'unfavourite', 'mute', 'unmute'):
                return True  # can always fave/boost public/unlisted toots

        if obj.visibility in ('direct',):
            # for direct toots, only original creator can reblog
            # This could be IsAllowedToReblog
            if action in ('reblog', 'unreblog'):
                return False  # creator catch-all is above
            # This could be IsAllowedToFavorite
            elif action in ('favourite', 'unfavourite'):
                # only those mentioned in a DM can fave it
                return obj.mentions_person(user.person)

        if obj.visibility in ('private',):
            # the 'un' methods are allowed; they will fail later if the object doesn't exist
            # This could just be IsAllowedToSee
            if action in ('unreblog', 'unfavourite'):
                return True
            # This could be IsAllowedToReblog
            elif action in ('reblog',):
                # only the owner of a followers-only post can reblog it
                return False
            # This could be IsAllowedToFavorite
            elif action in ('favourite',):
                # only followers of the ower can fave a post
                return user.person.is_following(obj.creator)
        
        return False


# Several types of Notifications can be triggered by the creation of statuses.
# For the Notifications endpoint to serialize these correctly, we must inform the NotificationSerializer
# about these notifications and how to include them in the Notifications payload.
# This is specific to the Mastodon API, and its choice to use different field names for different
# notification types.

# Mentions include the Status in the 'status' field.
NotificationSerializer.register_notification('microblog', notification_type='mention', content_type=Status, serializer=StatusSerializer, field_name='status')

# Reblogs include the Reblog version of the Status in the 'status' field.
NotificationSerializer.register_notification('microblog', notification_type='reblog', content_type=Status, serializer=StatusSerializer, field_name='status')

# Favourites include the original Status being favorited in the 'status' field.
NotificationSerializer.register_notification('microblog', notification_type='favourite', content_type=Status, serializer=StatusSerializer, field_name='status')


class StatusViewSet(viewsets.ModelViewSet):
    authentication_classes = (OAuth2Authentication, SessionAuthentication)
    serializer_class = StatusSerializer
    queryset = Status.objects.all()
    permission_classes = (IsOwnerOrCanViewStatus,)

    def _create_response(self, status, status_code=HTTP_200_OK):
        """
        DRY creating a response using a single Status object.
        """
        serializer = self.get_serializer(instance=status)
        return Response(serializer.data, status=status_code)

    def create(self, request, *args, **kwargs):
        """
        Creates a new Status (toot).
        
        @TODO: when posting a reply, ensure that creator can view the status being replied to
        """
        data = request.data
        
        # translate fields, as StatusSerializer for the timelines API is sufficient but the field
        # names don't match.
        data['content'] = data['status']

        # associate Status with logged-in User's Person.
        # TODO: when multiple Person per account supported, allow front-end to specify which Person
        data['account'] = {'acct': request.user.person.acct}
        
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=HTTP_201_CREATED)

    @action(methods=['POST'], detail=True)
    def mute(self, request, pk):
        status = self.get_object()
        thread = status.thread
        ThreadMute.objects.get_or_create(muter=request.user.person, thread=thread)
        return self._create_response(status, HTTP_200_OK)  # Todo: check permissions on outgoing status

    @action(methods=['POST'], detail=True)
    def unmute(self, request, pk):
        status = self.get_object()
        thread = status.thread
        mute = get_object_or_404(ThreadMute, muter=request.user.person, thread=thread)
        mute.delete()
        return self._create_response(status, HTTP_200_OK)  # Todo: check permissions on outgoing status

    @action(methods=['POST'], detail=True)
    def reblog(self, request, pk):
        status = self.get_object()  # should check permissions

        # Special cases:  DMs simply cannot be reblogged, even if you own it
        if status.visibility == 'direct':
            return Response([], status=HTTP_403_FORBIDDEN)

        reblog, created = Status.objects.get_or_create_reblog(status, request.user.person)

        if created:
            status_code = HTTP_201_CREATED
        else:
            status_code = HTTP_200_OK
        return self._create_response(reblog, status_code)

    @action(methods=['POST'], detail=True)
    def unreblog(self, request, pk):
        """
        Deletes a reblog status.
        """
        # special case: we're bypassing the permissions module because this is much easier to read
        reblog = Status.objects.get(pk=pk)

        # Mastodon's unreblog takes the original reblog ID, not the reblog ID.  Get the reblog from that.
        if reblog.reblog is None:
            # will 404 if not found
            reblog = get_object_or_404(Status, reblog=reblog, creator=request.user.person)
        
        # check if we are allowed to delete this - we own it and it's a reblog
        if reblog.creator != request.user.person or reblog.reblog is None:
            return Response("Can't unreblog that", status=HTTP_403_FORBIDDEN)

        # otherwise, it's fine to unreblog
        serializer = self.get_serializer(instance=reblog)
        data = serializer.data
        
        reblog.delete()
        
        # TODO status code?
        return Response(data, status=HTTP_200_OK)

    @action(methods=['POST'], detail=True)
    def favourite(self, request, pk):
        status = self.get_object()  # checks permissions
        Status.objects.get_or_create_favorite(status=status, creator=request.user.person)

        serializer = self.get_serializer(instance=status)
        return Response(serializer.data, status=HTTP_200_OK)

    @action(methods=['POST'], detail=True)
    def unfavourite(self, request, pk):
        # not using get_object as this is much clearer:
        status = Status.objects.get(pk=pk)
        Favorite.objects.filter(creator=request.user.person,
                                status=status).delete()
        serializer = self.get_serializer(instance=status)
        return Response(serializer.data, status=HTTP_200_OK)

    @action(methods=['POST'], detail=True)
    def pin(self, request, pk):
        """
        Create a StatusPin object for a given status.
        Permission checks should automatically prevent pinning a status that isn't yours.
        """
        status = self.get_object()
        if status.is_pinned():  # already pinned, noop
            return self._create_response(status)

        if status.reblog:
            # can't pin a reblog
            return Response({}, status=HTTP_403_FORBIDDEN)  # TODO: error message
            
        if status.visibility not in ('public', 'unlisted'):
            return Response({}, status=HTTP_403_FORBIDDEN)  # TODO: error message

        # enforce limit on number of pinned statuses
        limit = getattr(settings, 'STATUS_PIN_LIMIT', 4)
        existing_count = StatusPin.objects.filter(person=request.user.person).count()
        if existing_count >= limit:
            return Response({}, status=HTTP_403_FORBIDDEN)  # TODO: error message
        
        status.create_pin()

        return self._create_response(status)

    @action(methods=['POST'], detail=True)
    def unpin(self, request, pk):
        status = self.get_object()  # will handle permission checks automatically; see tests
        status.delete_pin()

        return self._create_response(status)

    @action(methods=['GET'], detail=True)
    def context(self, request, pk):
        """
        Gets the ancestors / descendants of a status using the threading model.
        """
        status = self.get_object()
        
        payload = {
            'ancestors': StatusSerializer(instance=status.ancestors, many=True).data,
            'descendants': StatusSerializer(instance=status.descendants, many=True).data
            }
        return Response(payload, status=HTTP_200_OK)

    @action(methods=['GET'], detail=True)
    def reblogged_by(self, request, pk):
        """
        Returns a paginated array of accounts that reblogged a given status.
        """
        status = self.get_object()  # handles permission checks
        reblogs = status.reblogs.all()
        queryset = Person.objects.filter(pk__in=reblogs.values_list('creator'))
        queryset = queryset.order_by('id')
        
        return LiveResponseHeaderPagination._generate_paginated_response(request,
                                                                         queryset,
                                                                         PersonSerializer,
                                                                         default_limit=40)

    @action(methods=['GET'], detail=True)
    def favourited_by(self, request, pk):
        """
        Returns a paginated array of accounts that favourited a given status.
        """
        status = self.get_object()
        favorites = status.favorites.all()
        queryset = Person.objects.filter(pk__in=favorites.values_list('creator'))
        queryset = queryset.order_by('id')
        
        return LiveResponseHeaderPagination._generate_paginated_response(request,
                                                                         queryset,
                                                                         PersonSerializer,
                                                                         default_limit=40)

class AccountStatusView(generics.ListAPIView):
    serializer_class = StatusSerializer
    queryset = Status.objects.all()
    permission_classes = (IsOwnerOrCanViewStatus, )
    pagination_class = LiveResponseHeaderPagination

    def get(self, request, pk):
        """
        GET /api/v1/accounts/:id/statuses - get statuses by an account.  paginated.
        
        Despite being under the 'accounts' namespace, this really doesn't belong in the social app.
        This has to be a separate View for routing, though.
        
        @TODO: Sanity check permissions.  permission_classes might not work here.
        """
        person = Person.objects.get(pk=pk)
        statuses = Status.objects.filter(creator=person)

        if get_boolean_query_param(request, 'pinned'):
            pinned_statuses = person.status_pins.all().values_list('status')
            statuses = statuses.filter(pk__in=pinned_statuses)

        if get_boolean_query_param(request, 'exclude_replies'):
            statuses = statuses.filter(in_reply_to=None)

        return LiveResponseHeaderPagination._generate_paginated_response(request, statuses, StatusSerializer)
