from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType

from microblog.models import Status, Mention, Favorite
from social.models import Notification

@receiver(post_save, sender=Status, dispatch_uid="reblog_notify")
def reblog_notify(sender, instance, created, raw, *args, **kwargs):
    """
    When somebody reblogs your status, you get a Notification about it.
    
    sender: the class of the object that was saved (i.e. the model)
    instance: the object that was saved (should be a Status)
    created: true if the object was just created, false if it was updated
    raw: true if this is a fixture import
    """
    if sender != Status or raw or not created:
        return  # don't notify on imports or edits.

    if not instance.reblog:  # we shouldn't be able to get here
        return

    # You can post a status and get numerous reblogs, and a notification for each.  This means
    # the related object needs to be the reblog, not the original status.  However, we need the
    # original status's creator to know who to notify.
    original_status = instance.reblog
    original_poster = original_status.creator

    content_type = ContentType.objects.get_for_model(instance)

    Notification.objects.get_or_create(app_label='microblog',
                                       notification_type='reblog',
                                       recipient=original_poster,
                                       sender=instance.creator,
                                       content_type=content_type,
                                       object_id=instance.id)

@receiver(post_save, sender=Mention, dispatch_uid="mention_notify")
def mention_notify(sender, instance, created, raw, *args, **kwargs):
    """
    When somebody mentions you in a status, you get a Notification about it.

    sender: the class of the object that was saved (i.e. the model)
    instance: the object that was saved (should be a Mention)
    created: true if the object was just created, false if it was updated
    raw: true if this is a fixture import
    """
    if sender != Mention or raw or not created:
        return  # don't notify on imports or edits

    # instance is the Mention object, but the notification is about its Status
    status = instance.status
    mentioned = instance.mentioned  # the person who was mentioned

    content_type = ContentType.objects.get_for_model(status)  # we notify for the Status, not the Mention

    Notification.objects.get_or_create(app_label='microblog',
                                       notification_type='mention',
                                       recipient=mentioned,
                                       sender=status.creator,
                                       content_type=content_type,
                                       object_id=status.id)

@receiver(post_save, sender=Favorite, dispatch_uid="favorite_notify")
def favorite_notify(sender, instance, created, raw, *args, **kwargs):
    """
    When somebody favorites your in a Status, you get a Notification about it - even if
    they favorited a reblog of your Status.

    # TODO: Test sending notifications when a reblog is favorited.

    sender: the class of the object that was saved (i.e. the model)
    instance: the object that was saved (should be a Favorite)
    created: true if the object was just created, false if it was updated
    raw: true if this is a fixture import
    """
    if sender != Favorite or raw or not created:
        return  # don't notify on imports or edits

    # instance is the Mention object, but the notification is about its Status
    status = instance.status
    favoriter = instance.creator  # the person who created the favorite

    content_type = ContentType.objects.get_for_model(status)

    Notification.objects.get_or_create(app_label='microblog',
                                       notification_type='favourite',
                                       recipient=status.creator,
                                       sender=favoriter,
                                       content_type=content_type,
                                       object_id=status.id)
