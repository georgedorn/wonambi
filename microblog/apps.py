from django.apps import AppConfig


class MicroblogConfig(AppConfig):
    name = 'microblog'

    def ready(self):
        super().ready()

        # explicitly import signals, because Django.
        from microblog import signals  # noqa
