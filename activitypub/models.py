from django.db import models


class APObject(models.Model):
    """
    Base ActivityPub Object that nearly every AP model inherits from.
    """
    object_id = models.URLField(null=True)
    # type = models.CharField(max_length=256)

    class Meta:
        abstract = True


class APActor(APObject):
    """
    Abstract base class for Actors.
    See https://www.w3.org/TR/activitypub/#actor-objects
    
    Many fields are optional.  Only the required ones are defined here.

    Additional optional fields that might be useful for an implementation:

    preferredUsername = models.TextField(help_text="Actor's preferred username for display purposes")
    summary = models.TextField(help_text="Actor's summary, e.g. public profile")


    TODO:
        Any non-URL string in this object may support i18n.
        For example, if `name` is implemented with i18n support, it will be replaced in the API with:
            nameMap: {'en': "My Name", 'fr': "My Name In French", ...}
        See https://www.w3.org/TR/activitystreams-core/#naturalLanguageValues

    It's probably not the job of this abstract base class to specify how to implement i18n, but another
    library such as django-modeltranslation is likely the best bet.  Document how to do this, preferably
    without modifying either this class or the concrete subclass.
    """
    
    class Meta:
        abstract = True  # known subclasses are e.g. Application, Group, Org, Person, Service

    @property
    def inbox(self):
        """
        A reference (URL) to an ActivityStreams OrderedCollection comprised of all messages received
        by the actor.
        """
        raise NotImplementedError("inbox property must be defined in inheriting class")

    @property
    def outbox(self):
        """
        A reference (URL) to an ActivityStreams OrderedCollection comprised of all the messages produced
        by the actor.
        """
        raise NotImplementedError("outbox property must be defined in inheriting class")

    @property
    def url(self):
        """
        A link to the actor's "profile web page" if not equal to the value of `object_id`.
        Define this if necessary, otherwise defaults to self.object_id
        """
        return self.object_id

    @property
    def _name(self):
        """
        Actor's display name property; this could be username, name, or preferredUsername.  One of these
        should be defined, or this property should be overridden.

        See NOTE section regarding ActivityStream use of ActivityPub Actors
        in https://www.w3.org/TR/activitypub/#actor-objects
        """
        if hasattr(self, 'preferredUsername'):
            return self.preferredUsername
        if hasattr(self, 'name'):
            return self.name
        if hasattr(self, 'username'):
            return self.username
        raise NotImplementedError("_name property must be defined for ActivityStream access to Actors")


class APSocialActor(APActor):
    """
    The base class APActor is the barest skeleton of ActivityPub Actor.  Several 'SHOULD' fields
    are added in APSocialActor, for those assumed to be used by any social network-like implementation.

    Additional properties that applications MAY wish to implemenent:

    streams - a list of references (URLs) to other collections of interest
    endpoints - a map of useful endpoints for this actor, such as:
        proxyUrl
        oauthAuthorizationEndpoint
        oauthTokenEndpoint
        

    """

    class Meta(APActor.Meta):
        abstract = True

    @property
    def following(self):
        """
        A link (URL) to an ActivityStreams collection of actors this actor is following.
        """
        raise NotImplementedError("Social Network Actors SHOULD implement the `following` property")

    @property
    def followers(self):
        """
        A link (URL) to an ActivityStreams collection of actors that follow this actor.
        """
        raise NotImplementedError("Social Network Actors SHOULD implement the `followers` property")

    @property
    def liked(self):
        """
        A link (URL) to an ActivityStreams collection of objects this actor has liked.
        """
        raise NotImplementedError("Social Network Actors SHOULD implement the `liked` property")

    @property
    def endpoints(self):
        """
        A map of other useful endpoints for this actor, returned as a map (dict).
        """
        _endpoints = {}
        for key in ('proxyUrl', 'oauthAuthorizationEndpoint', 'oauthTokenEndpoint', 'provideClientKey',
                    'signClientKey', 'sharedInbox'):
            if hasattr(self, key):
                _endpoints[key] = getattr(self, key)
        return _endpoints or None

