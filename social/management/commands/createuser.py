"""
Management utility to create users.

This is largely a subclass of Django's built-in `createsuperuser` command
(django.contrib.auth.management.commands.createsuperuser) with noted additions for collecting
the username field for the Person object.
"""
import getpass
import sys

from django.contrib.auth.management import get_default_username
from django.contrib.auth.management.commands import createsuperuser  # noqa
from django.contrib.auth.password_validation import validate_password
from django.core import exceptions
from django.core.management.base import CommandError
from django.utils.text import capfirst
from social.models import Person


# This whole command can only be tested modestly; don't include in coverage tests
class Command(createsuperuser.Command):   # pragma:  no cover
    help = 'Used to create a normal user.'
    requires_migrations_checks = True
    stealth_options = ('stdin',)

    def add_arguments(self, parser):
        super().add_arguments(parser)
        ### Custom addition to default superuser args
        parser.add_argument(
            '--person_username', action='store', dest='person_username',
            default=None,
            help='Specifies the username for the Person.'
        )

    def handle(self, *args, **options):
        username = options[self.UserModel.USERNAME_FIELD]
        database = options['database']

        person_username = options['person_username']

        # If not provided, create the user with an unusable password
        password = None
        user_data = {}
        # Same as user_data but with foreign keys as fake model instances
        # instead of raw IDs.
        fake_user_data = {}
        verbose_field_name = self.username_field.verbose_name

        # Do quick and dirty validation if --noinput
        if not options['interactive']:
            try:
                if not username:
                    raise CommandError("You must use --%s with --noinput." % self.UserModel.USERNAME_FIELD)
                username = self.username_field.clean(username, None)

                for field_name in self.UserModel.REQUIRED_FIELDS:
                    if options[field_name]:
                        field = self.UserModel._meta.get_field(field_name)
                        user_data[field_name] = field.clean(options[field_name], None)
                    else:
                        raise CommandError("You must use --%s with --noinput." % field_name)
            except exceptions.ValidationError as e:
                raise CommandError('; '.join(e.messages))

            # CUSTOM addition to get Person.username from commandline args.
            user_data['person_username'] = person_username

        else:
            # Prompt for username/password, and any other required fields.
            # Enclose this whole thing in a try/except to catch
            # KeyboardInterrupt and exit gracefully.
            default_username = get_default_username()
            try:

                if hasattr(self.stdin, 'isatty') and not self.stdin.isatty():
                    raise NotRunningInTTYException("Not running in a TTY")

                # Get a username
                while username is None:
                    input_msg = capfirst(verbose_field_name)
                    if default_username:
                        input_msg += " (leave blank to use '%s')" % default_username
                    username_rel = self.username_field.remote_field
                    input_msg = '%s%s: ' % (
                        input_msg,
                        ' (%s.%s)' % (
                            username_rel.model._meta.object_name,
                            username_rel.field_name
                        ) if username_rel else ''
                    )
                    username = self.get_input_data(self.username_field, input_msg, default_username)
                    if not username:
                        continue
                    if self.username_field.unique:
                        try:
                            self.UserModel._default_manager.db_manager(database).get_by_natural_key(username)
                        except self.UserModel.DoesNotExist:
                            pass
                        else:
                            self.stderr.write("Error: That %s is already taken." % verbose_field_name)
                            username = None

                if not username:
                    raise CommandError('%s cannot be blank.' % capfirst(verbose_field_name))

                # CUSTOM: Prompt for the 'username' field of the Person model
                person_username_field = Person._meta.get_field('username')
                person_username = self.get_input_data(person_username_field,
                                                      'Person username (e.g. @foo, without domain): ')
                person_username = person_username.replace('@', '')  # modest cleanup

                user_data['person_username'] = person_username
                # End CUSTOM

                for field_name in self.UserModel.REQUIRED_FIELDS:
                    field = self.UserModel._meta.get_field(field_name)
                    user_data[field_name] = options[field_name]
                    while user_data[field_name] is None:
                        message = '%s%s: ' % (
                            capfirst(field.verbose_name),
                            ' (%s.%s)' % (
                                field.remote_field.model._meta.object_name,
                                field.remote_field.field_name,
                            ) if field.remote_field else '',
                        )
                        input_value = self.get_input_data(field, message)
                        user_data[field_name] = input_value
                        fake_user_data[field_name] = input_value

                        # Wrap any foreign keys in fake model instances
                        if field.remote_field:
                            fake_user_data[field_name] = field.remote_field.model(input_value)

                # Get a password
                while password is None:
                    password = getpass.getpass()
                    password2 = getpass.getpass('Password (again): ')
                    if password != password2:
                        self.stderr.write("Error: Your passwords didn't match.")
                        password = None
                        # Don't validate passwords that don't match.
                        continue

                    if password.strip() == '':
                        self.stderr.write("Error: Blank passwords aren't allowed.")
                        password = None
                        # Don't validate blank passwords.
                        continue

                    try:
                        validate_password(password2, self.UserModel(**fake_user_data))
                    except exceptions.ValidationError as err:
                        self.stderr.write('\n'.join(err.messages))
                        response = input('Bypass password validation and create user anyway? [y/N]: ')
                        if response.lower() != 'y':
                            password = None

            except KeyboardInterrupt:  # pragma:  no cover
                self.stderr.write("\nOperation cancelled.")
                sys.exit(1)

            except createsuperuser.NotRunningInTTYException:  # pragma:  no cover
                self.stdout.write(
                    "User creation skipped due to not running in a TTY. "
                    "You can run `manage.py createuser` in your project "
                    "to create one manually."
                )

        if username:
            user_data[self.UserModel.USERNAME_FIELD] = username
            user_data['password'] = password
            self.UserModel._default_manager.db_manager(database).create_user(**user_data)
            if options['verbosity'] >= 1:
                self.stdout.write("User created successfully.")
