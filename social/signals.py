from django.db.models.signals import post_save
from django.dispatch import receiver

from social.models import PersonFollow, Notification
from django.contrib.contenttypes.models import ContentType


@receiver(post_save, sender=PersonFollow, dispatch_uid="follower_notify")
def follower_notify(sender, instance, created, raw, *args, **kwargs):
    """
    When somebody follows you, you get a Notification about it.

    sender: the class of the object that was saved (i.e. the model)
    instance: the object that was saved (should be a PersonFollow)
    created: true if the object was just created, false if it was updated
    raw: true if this is a fixture import
    """
    # Don't send notifications for modified or raw new follows
    if sender != PersonFollow or raw or not created:
        return  # nope
    follower = instance.follower
    followed = instance.followed

    # GFKs + get_or_create doesn't work with the GenericRelation, not going to debug why
    content_type = ContentType.objects.get_for_model(follower)

    Notification.objects.get_or_create(app_label='social',
                                       notification_type='follow',
                                       recipient=followed,
                                       sender=follower,
                                       content_type=content_type,
                                       object_id=follower.id)
    