from wonambi.utils.router import OptionalSlashRouter

from social.api import PersonViewset, NotificationsViewSet, FollowRequestViewSet


router = OptionalSlashRouter()
router.register(r'accounts', PersonViewset)
router.register(r'notifications', NotificationsViewSet)
router.register(r'follow_requests', FollowRequestViewSet)

urlpatterns = router.urls
