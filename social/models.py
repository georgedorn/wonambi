from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from wonambi.utils.models import TimestampedModel
from django.db.models.query_utils import Q
from django.db.models.manager import BaseManager
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager, AbstractUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError



class AccountManager(UserManager):
    """
    Override default UserManager behavior to remove the 'username' field that we are not using
    for Django - the username field is on the Person model.
    """
    def _create_user(self, email, password, person_username=None, **extra_fields):
        """
        Create and save a user with the given email and password.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db, person_username=person_username)

        return user

    def create_user(self, email=None, password=None, person_username=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, person_username, **extra_fields)

    def create_superuser(self, email, password, person_username=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, person_username, **extra_fields)


class Account(AbstractBaseUser, PermissionsMixin, TimestampedModel):
    """
    Per-human account details.  Not the same as a 'User' or 'Person' Actor in AP; you log in
    via Account details, but then can choose (or default to) a specific Actor to behave as.

    Creating a new Account may automatically create an Actor with the same username.
    """
    email = models.EmailField(unique=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    username = None

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )


    objects = AccountManager()

    def save(self, person_username=None, *args, **kwargs):
        super().save(*args, **kwargs)
        AccountStats.objects.get_or_create(account=self, defaults={'last_sign_in_at': timezone.now(),
                                                                   'current_sign_in_at': timezone.now()})
        if person_username:
            Person.objects.get_or_create(account=self, username=person_username)

class AccountStats(TimestampedModel):
    account = models.OneToOneField(Account, null=False, unique=True, on_delete=models.CASCADE)
    sign_in_count = models.IntegerField(default=0)
    last_sign_in_at = models.DateTimeField(default=None, null=True)
    current_sign_in_at = models.DateTimeField(default=None, null=True)
    # confirmed_at = models.DateTimeField()
    # confirmation_sent_at = models.DateTimeField()
    # unconfirmed_email = models.EmailField(null=True)


class PersonQuerySet(models.QuerySet):

    def search(self, query):
        """
        Filter accounts by a query, which looks in username, domain and display name.
        """
        filters = (Q(username__icontains=query) |
                    Q(domain__icontains=query) |
                    Q(preferred_username__icontains=query))
        return self.filter(filters)


class Person(TimestampedModel):
    """
    A Person is an Actor that posts statuses, follows other people, etc.
    An Account may have multiple Person objects, each representing another Fediverse presence.

    Where possible, these fields are derived from ActivityPub; if there is a Mastodon equivalent, that
    will be noted as well.
    """
    # Note:  Don't confuse this field with Mastodon's notion of an account.  This is strictly for
    # logging into the instance; Mastodon's `acct` field corresponds with this Person object, not
    # Person.account.
    account = models.OneToOneField(Account, on_delete=models.CASCADE, null=True,
                                   help_text="Login account for this Person. Null if from another instance.")
    username = models.CharField(max_length=256, blank=False, null=False)
    domain = models.CharField(max_length=256, default=None, null=True, blank=True,
                              help_text="Domain of this user's instance; defaults to settings.DOMAIN")
    is_bot = models.BooleanField(default=False)
    # AP's preferredUsername; Mastodon calls this display_name
    preferred_username = models.CharField(max_length=256)

    # These 'count' fields are provided in API requests; they're mainly for display and don't necessarily
    # correspond to counts in the local db, particularly when this is foreign Person
    followers_count = models.IntegerField(default=0,
                                          help_text="Number of followers")
    following_count = models.IntegerField(default=0,
                                          help_text="Number of follows")
    statuses_count = models.IntegerField(default=0,
                                         help_text="Number of posts by Person")

    # Mastodon calls this `Account.note`
    summary = models.TextField(help_text="About this Person")
    locale = models.CharField(max_length=32)

    locked = models.BooleanField(default=False,
                                 help_text="True when Person cannot be followed without approval")
    note = models.TextField(help_text="Biography of user", default="")
    url = models.URLField(default="")

    following = models.ManyToManyField('Person', through="PersonFollow",
                                       symmetrical=False, through_fields=('follower', 'followed'),
                                       related_name='followers')

    @property
    def acct(self):
        return "{USERNAME}@{DOMAIN}".format(USERNAME=self.username,
                                            DOMAIN=self.domain or settings.DOMAIN)

    objects = BaseManager.from_queryset(PersonQuerySet)()

    def is_following(self, other_person):
        return self in other_person.followers.all()

    def save(self, *args, **kwargs):
        self.username = self.username.strip()
        self.username = self.username.replace('@', '')  # sanity check
        if not self.username:
            raise ValidationError("Person.username may not be blank.")
        if not self.domain:
            self.domain = settings.DOMAIN
        return super().save(*args, **kwargs)

    class Meta:
        unique_together = [('username', 'domain'),]


class PersonFilteredLanguage(TimestampedModel):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    filtered_language = models.CharField(max_length=32)


class PersonFollow(TimestampedModel):
    """
    Tracks every AP Actor this Person follows.  Person may either be a proper Person object,
    or an identifier from another instance may be used instead.  One or the other is required.
    """
    follower = models.ForeignKey(Person, help_text="The actor doing the following", on_delete=models.CASCADE,
                                 related_name='+')
    followed = models.ForeignKey(Person, help_text="The actor being followed, if on this instance.",
                                 on_delete=models.CASCADE, related_name='+')


class PersonFollowRequest(TimestampedModel):
    """
    Tracks a request for an AP actor to follow a Person.
    """
    follower = models.ForeignKey(Person, help_text="The actor requesting to follow", on_delete=models.CASCADE,
                                 related_name='requestors')
    followed = models.ForeignKey(Person, help_text="The actor that is recieving the request",
                                 on_delete=models.CASCADE, related_name='requestees')

    def authorize(self):
        """
        Converts this PersonFollowRequest into a PersonFollow and deletes this object.
        """
        follow, _ = PersonFollow.objects.get_or_create(follower=self.follower, followed=self.followed)
        
        # Delete all, in case we somehow got a duplicate.
        PersonFollowRequest.objects.filter(follower=self.follower, followed=self.followed).delete()

        return follow

class PersonBlock(TimestampedModel):
    """
    A block from one Person on another.  The UI might present the option to block from every Person the
    current Account contains.
    """
    blocker = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='blocks')
    blockee = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='blockers')


class PersonDomainBlock(TimestampedModel):
    """
    A block from one Person to another entire domain (instance).
    """
    blocker = models.ForeignKey(Person, on_delete=models.CASCADE)
    domain = models.CharField(max_length=256, help_text="Domain from which to ignore all activity.")


class Notification(TimestampedModel):
    """
    Notifications for a recipient Person.  Notifications can (and usually do) contain a reference
    to another object, known as the `content_object` and implemented using a Generic Foreign Key
    for maximum compatibility with future apps.

    Example notifications and their content objects:
    Follow - content type is Person, content object is the Person doing the following
    Mention - content type is Status (or future type), content object is the message mentioning the recipient.
    """
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey(ct_field='content_type',
                                       fk_field='object_id',)

    app_label = models.CharField(max_length=32,
                                 help_text="The app sending the notification; used for namespacing")
    notification_type = models.CharField(
        max_length=32,
        help_text="The AP type to explain this notification - e.g.s 'mention' or 'reblog' or 'follow'",
        blank=False, null=False
    )

    sender = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, default=None,
        help_text="The Person who caused the notification to be sent; null is possible")
    recipient = models.ForeignKey(Person, on_delete=models.CASCADE,
       help_text="The recipient of the Notification, distinct from the Person doing the notifying.",
       related_name='notifications')
