from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication

from wonambi.utils.api import PermissionsPerMethodMixin
from wonambi.utils.pagination import LiveResponseHeaderPagination

from social.models import PersonFollow, Notification, PersonFollowRequest
from social.serializers import PersonRelationshipSerializer, NotificationSerializer

from .models import Person
from .serializers import PersonSerializer
from django.contrib.auth.models import AnonymousUser

# Register the various Notifications that the 'account' API can send:
NotificationSerializer.register_notification(app_label='social',
                                             notification_type='follow',
                                             content_type=Person,
                                             serializer=PersonSerializer,
                                             field_name='account')


class PersonViewset(PermissionsPerMethodMixin, viewsets.ModelViewSet):
    authentication_classes = (OAuth2Authentication, SessionAuthentication)
    queryset = Person.objects.all()
    serializer_class = PersonSerializer
    pagination_class = LiveResponseHeaderPagination

    @action(detail=False, methods=['get'])
    def search(self, request):
        """
        GET /api/v1/accounts/search?q=monkey

        Search for accounts by keyword 'q' query param.
        Used by mastodon front end when mentioning somebody in a toot (autocomplete) or when adding
        somebody to a list.
        """
        query = request.query_params.get('q', None)
        if not query:
            return Response(data=[], status=400)
        results = self.get_queryset().search(query)

        if request.query_params.get('following', '').lower() in ('true', '1'):
            following = request.user.person.following.all()
            results = results.intersection(following)

        limit = int(request.query_params.get('limit', 40))

        serializer = self.get_serializer(results[0:limit], many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    @permission_classes((IsAuthenticated,),)
    def verify_credentials(self, request):
        """
        Barebones implementation of /api/v1/accounts/verify_credentials

        Note that this violates the separation of accounts and AP Person objects.
        
        TODO: This should include an extra 'source' attribute containing more keys.  See
        https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#getting-the-current-user
        """
        person = request.user.person
        serializer = self.get_serializer(instance=person)
        return Response(serializer.data)

    @action(detail=False, methods=['patch'])
    @permission_classes((IsAuthenticated,),)
    def update_credentials(self, request):
        """
        Essentially a clone of update() except using current logged-in-user by default.

        Note that this violates the separation of accounts and AP Person objects.
        """
        instance = request.user.person

        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(data=serializer.data,
                        status=200)

    @action(detail=True, methods=['get'])
    def followers(self, request, pk):
        """
        GET /api/v1/accounts/:id/followers

        Accounts which follow the given account.

        Returns paginated array of Account

        @TODO: support hiding followers
        """
        person = self.get_object()
        accounts = person.followers.all().order_by('-pk')

        paginator = self.pagination_class(request, accounts)
        paginated_statuses = paginator.apply_pagination()

        serializer = self.get_serializer(paginated_statuses, many=True)

        return paginator.get_paginated_response(serializer.data)

    @action(detail=True, methods=['get'])
    def following(self, request, pk):
        """
        GET /api/v1/accounts/:id/following
        
        Accounts the given account follows.
        
        Return paginated array of Accounts

        @TODO: Support hiding following
        """
        person = self.get_object()
        accounts = person.following.all().order_by('-pk')

        paginator = self.pagination_class(request, accounts)
        paginated_statuses = paginator.apply_pagination()

        serializer = self.get_serializer(paginated_statuses, many=True)

        return paginator.get_paginated_response(serializer.data)

    @action(detail=True, methods=['post'])
    @permission_classes((IsAuthenticated,),)
    def follow(self, request, pk):
        """
        POST /api/v1/accounts/:id/follow

        Causes currently-logged-in user to follow another account by ID.

        Returns `Relationship` object.
        """
        follower = request.user.person  # doing the action
        followed = self.get_object()

        if followed.locked:
            # Person has enabled Follow Requests
            PersonFollowRequest.objects.get_or_create(follower=follower, followed=followed)
        else:
            PersonFollow.objects.get_or_create(follower=follower, followed=followed)

        serializer = PersonRelationshipSerializer(requestor=follower, instance=followed)
        return Response(serializer.data)

    @action(detail=True, methods=['post'])
    @permission_classes((IsAuthenticated,),)
    def unfollow(self, request, pk):
        """
        POST /api/v1/accounts/:id/unfollow

        Causes currently-logged-in user to stop following another account by ID.

        Returns `Relationship` object.
        """
        follower = request.user.person  # doing the action
        followed = self.get_object()
        PersonFollow.objects.filter(follower=follower, followed=followed).delete()
        PersonFollowRequest.objects.filter(follower=follower, followed=followed).delete()

        serializer = PersonRelationshipSerializer(requestor=follower, instance=followed)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def relationships(self, request):
        """
        GET /api/v1/accounts/relationships
        
        Requires one or more `id` parameters in the query params, to specify which Person objects you
        want relationship data for.  If it is an array, for API compatibility with Mastodon frontends,
        this will accept ?id[]=1&id[]=2 etc, but the brackets are optional.

        Note that authentication is not required; following/follower/follower_requested
        will always be False if user is anonymous.

        @TODO: Implement relationship hiding
        """
        if 'id' in request.query_params:
            ids = request.query_params.getlist('id')
        elif 'id[]' in request.query_params:
            ids = request.query_params.getlist('id[]')  # hack to support rails/php-style arrays
        people = Person.objects.filter(pk__in=ids)

        if isinstance(request.user, AnonymousUser):
            requestor = None
        else:
            requestor = request.user.person
        serializer = PersonRelationshipSerializer(requestor=requestor, instance=people, many=True)
        return Response(serializer.data)


class FollowRequestViewSet(viewsets.ModelViewSet):
    """
    Endpoints for getting/managing FollowRequests.
    """
    serializer = PersonSerializer
    pagination_class = LiveResponseHeaderPagination
    queryset = PersonFollowRequest.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return PersonFollowRequest.objects.filter(followed=self.request.user.person).order_by('-created_at')

    def list(self, request, *args, **kwargs):
        """
        GET /api/v1/follow_requests returns an array of Person objects who have an outstanding
        follow request for logged-in user.
        """
        follow_requests = self.get_queryset().select_related('follower')
        people = [fr.follower for fr in follow_requests]  # should be no queries for this due to select_related()
        return LiveResponseHeaderPagination._generate_paginated_response(request,
                                                                         people,
                                                                         PersonSerializer)

    def _get_follow_request_by_person_id(self, pk):
        """
        IDs passed to this endpoint are Person ('account') IDs, not the IDs for the PersonFollowRequest
        objects. This takes a Person ID for the follower and assumes the followed person to be the
        logged-in user.
        """
        return get_object_or_404(PersonFollowRequest, follower=pk, followed=self.request.user.person)

    @action(detail=True, methods=['post'])
    def authorize(self, request, pk):
        follow_request = self._get_follow_request_by_person_id(pk)
        follow_request.authorize()
        return Response({})

    @action(detail=True, methods=['post'])
    def reject(self, request, pk):
        """
        Refuse a follow request.  This simply deletes the PersonFollowRequest object.
        """
        follow_request = self._get_follow_request_by_person_id(pk)
        follow_request.delete()
        return Response({})
        
        
class NotificationsViewSet(viewsets.ModelViewSet):
    """
    GET /api/v1/notifications to retrieve notifications for currently-logged-in user.
    """
    serializer_class = NotificationSerializer
    pagination_class = LiveResponseHeaderPagination
    queryset = Notification.objects.all()
    permission_classes = (IsAuthenticated,)

    class Meta:
        model = Notification
    
    def get_queryset(self):
        return Notification.objects.filter(recipient=self.request.user.person).order_by('-created_at')

    def list(self, request, *args, **kwargs):
        """
        Override default GET behavior to use custom pagination that doesn't quite work with
        DRF by default.
        """
        notes = self.get_queryset()
        # support exclude by type, with or without PHP-style brackets
        exclude_types = request.query_params.getlist('exclude_types[]')  \
                            or request.query_params.getlist('exclude_types')
        if exclude_types:
            notes = notes.exclude(notification_type__in=exclude_types)
        return LiveResponseHeaderPagination._generate_paginated_response(request,
                                                                         notes,
                                                                         NotificationSerializer)

    @action(detail=False, methods=['post'])
    def clear(self, request):
        """
        Deletes all of a user's notifications.
        """
        self.get_queryset().all().delete()
        return Response({})
