from django.apps import AppConfig


class SocialConfig(AppConfig):
    name = 'social'
    def ready(self):
        super().ready()

        # explicitly import signals, because Django.
        from social import signals  # noqa
