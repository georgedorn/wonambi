from django.conf import settings
from rest_framework import serializers

from .models import Person
from social.models import PersonFollow, Notification, PersonFollowRequest
from rest_framework.fields import empty
from rest_framework.exceptions import ValidationError
from django.contrib.contenttypes.models import ContentType


class AcctField(serializers.Field):
    """
    Custom field to handle compatibility with Mastodon's `acct` field.
    
    Use me with `source='*'`
    """
    def to_representation(self, obj):
        """
        Extract `acct` from username+domain combo.
        DRF reuses this method for serializing in both directions, from a Person object
        or an OrderedDict, leading to the two ugly branches below.
        """
        if isinstance(obj, Person):
            domain = getattr(obj, 'domain', None)
            username = getattr(obj, 'username')
        elif isinstance(obj, dict):
            domain = obj.get('domain', None)
            username = obj.get('username')
        return username + '@' + (domain or settings.DOMAIN)

    def to_internal_value(self, data):
        username, domain = data.split('@')
        return {
            'username': username,
            'domain': domain
        }
        
class PersonSerializer(serializers.ModelSerializer):
    acct = AcctField(source='*')
    avatar = serializers.SerializerMethodField(method_name='get_empty_url')
    avatar_static = serializers.SerializerMethodField(method_name='get_empty_url')
    header = serializers.SerializerMethodField(method_name='get_empty_url')
    header_static = serializers.SerializerMethodField(method_name='get_empty_url')
    bot = serializers.BooleanField(source='is_bot', required=False)
    display_name = serializers.CharField(source='preferred_username', required=False)
    emojis = serializers.SerializerMethodField()
    moved = serializers.SerializerMethodField(method_name='get_empty_url')
    fields = serializers.SerializerMethodField(method_name='get_empty_dict')
    

    class Meta:
        model = Person
        fields = ('id', 'username', 'acct', 'display_name', 'locked', 'created_at',
                  'followers_count', 'following_count', 'statuses_count',
                  'note', 'url', 'avatar', 'avatar_static', 'header', 'header_static',
                  'emojis', 'moved', 'bot', 'fields',
                  'updated_at')

    def get_empty_url(self, obj):
        return "http://nowhere.instance/path"

    def get_empty_dict(self, obj):
        return {}

    def get_emojis(self, obj):
        return []

    def create(self, validated_data):
        """
        We never want to try to create a new Person without checking for a dupe first.
        """
        return self.update_or_create(validated_data)

    def update_or_create(self, validated_data=None):
        """
        Handles creating new Person objects, or updating existing ones with new data.
        """
        data = validated_data or self.validated_data
        if 'acct' in data:
            del data['acct']  # ffs DRF
        defaults = {k: data[k] for k in self.fields if k in data}
        person, _ = Person.objects.update_or_create(
            username=data['username'],
            domain=data['domain'],
            defaults=defaults
        )
        return person

    def run_validation(self, data):
        """
        Handles mapping from mastodon fields to our db schema, for derived fields.
        """
        # DRF is weird about nested objects; it's already split `acct` into `username` and `domain`
        # but insists that `acct` be in the incoming payload (because required=True).
        # Perhaps there's a better way to signify that we're good, we've already got the data?

        if data is empty:
            data = {}

        if self.instance is not None:
            # If we have an instance already, we are updating, not creating.
            # in this case, explicitly disallow changing these fields
            data['acct'] = self.instance.acct
            data['username'] = self.instance.username
            data['domain'] = self.instance.domain
            return super().run_validation(data)

        # Everything below here pertains to creating new Person objects
        acct = data.get('acct', None)
        username = data.get('username', None)
        domain = data.get('domain', None)

        if not acct and 'username' not in data:
            raise serializers.ValidationError("username or acct required to create Person")

        if username and domain and not acct:
            data['acct'] = username + '@' + domain
        elif acct and not (username and domain):
            data['username'], data['domain'] = acct.split('@')

        return super().run_validation(data)


class PersonRelationshipSerializer(serializers.ModelSerializer):
    """
    Implementation of the Mastodon Relationship object:
    https://docs.joinmastodon.org/api/entities/#relationship
    
    This is not well-documented.  My best guess is that it always pertains to the current
    logged-in user, so request.user.person is a requirement for initialization.

    Given that each of these fields is controlled from a different endpoint, it is also my belief
    that this is a read-only object - i.e. you don't POST this object anywhere.
    """
    id = serializers.SerializerMethodField()
    following = serializers.SerializerMethodField()
    followed_by = serializers.SerializerMethodField()
    blocking = serializers.SerializerMethodField()
    muting = serializers.SerializerMethodField()
    muting_notifications = serializers.SerializerMethodField()
    requested = serializers.SerializerMethodField()
    domain_blocking = serializers.SerializerMethodField()
    showing_reblogs = serializers.SerializerMethodField()
    endorsed = serializers.SerializerMethodField()

    class Meta:
        model = Person
        fields = (
            'id',
            'following',
            'followed_by',
            'blocking',
            'muting',
            'muting_notifications',
            'requested',
            'domain_blocking',
            'showing_reblogs',
            'endorsed'
        )

    def __init__(self, requestor, *args, **kwargs):
        self.requestor = requestor
        super().__init__(*args, **kwargs)

    def get_id(self, obj):
        return obj.pk

    def get_following(self, obj):
        """
        Returns True if logged-in person follows the provided Person.
        """
        if self.requestor is None:
            return False
        return PersonFollow.objects.filter(follower=self.requestor, followed=obj).exists()

    def get_followed_by(self, obj):
        """
        Return True if logged-in person is followed by the provided Person.
        """
        if self.requestor is None:
            return False
        return PersonFollow.objects.filter(follower=obj, followed=self.requestor).exists()
    
    def get_blocking(self, obj):
        """
        Returns True if logged-in person is blocking the provided Person.
        @TODO: fill this in when blocking is implemented.
        """
        return False

    def get_muting(self, obj):
        """
        Returns True if logged-in person is muting the provided Person.
        @TODO: fill this in when muting is implemented.
        """
        return False
    
    def get_muting_notifications(self, obj):
        """
        Returns True if logged-in person is muting notifications from the provided Person.
        @TODO: fill this in when muting is implemented.
        """
        return False

    def get_requested(self, obj):
        """
        Returns True if logged-in person has requested to follow the provided Person.
        @TODO: fill this in when follow requests are implemented.
        """
        if self.requestor is None:
            return False
        return PersonFollowRequest.objects.filter(follower=self.requestor, followed=obj).exists()

    def get_domain_blocking(self, obj):
        """
        Returns True if logged-in person is domain-blocking the masto instance of the provided Person.
        @TODO: fill this in when domain blocking is implemented.
        """
        return False

    def get_showing_reblogs(self, obj):
        """
        Returns True if logged-in person is showing reblogs by the provided Person. (?)
        @TODO: fill this in when this feature is implemented.
        """
        return False

    def get_endorsed(self, obj):
        """
        Returns True if logged-in person has endorsed (?) the provided Person.
        @TODO: fill this in when muting is implemented.
        """
        return False


class NotificationSerializer(serializers.Serializer):
    """
    Class that generically handles outputting the right type for any given Notification.
    E.g. when the notification is about a Status being favorited or reblogged or the account is mentioned
    in it, the `status` field will use the StatusSerializer.
    """
    notification_registry = {}
    id = serializers.IntegerField()
    type = serializers.CharField(source='notification_type')
    app_label = serializers.CharField()
    created_at = serializers.DateTimeField()
    
    class Meta:
        fields = ("id",
                  "type",
                  "created_at",
                  )
        model = Notification

    @classmethod
    def register_notification(cls, app_label, notification_type, content_type, serializer, field_name):
        """
        Tells NotificationSerializer how to serialize a kind of notification.
        During retrieval, this association is used to append fields (using `field_name`,
        e.g. "status" or "account") containing serialized data using `serializer`.

        None of the examples here are exhaustive; future apps may register their own notifications.

        Parameters:
        - notification_type: used by the client to decide how to display the notification.
            example values: 'follow', 'mention', 'reblog', 'favourite' 
        - app_label: used to namespace notification_type to avoid collisions.  Allows two apps to use
            'mention' type, but embed different nested data (e.g. a microblog post vs a tumblr post)
        - content_type: the model to nest within this entry in the returned payload.
            example values: 'Status', 'Person'
        - serializer: the serializer to use when nesting this entry in the payload
            example values: 'StatusSerializer', 'PersonSerializer'
        - field_name: the name of the nested field within the payload
            example values: 'status', 'account'

        During serialization, each `Notification.notification_type` is used to look up the rest of the
        implementation for embedding the nested data.

        A note about separation of concerns:
        - This Serializer is specific to the Mastodon UI.  The Notification model, however, is generic
        and can support other types with no changes.  Therefore Notification creation should happen near
        the model layer (or post_save signals), but all code pertaining to mangling that fairly clean model
        into the form Mastodon expects should happen within this class or the API views making use of it.
        """
        notification_key = (app_label, notification_type)
        # sanity check: don't allow apps to clobber existing registrations
        if notification_key in cls.notification_registry:
            raise RuntimeError("(%s, %s) already registered, cannot re-register"
                               % (app_label, notification_type))

        cls.notification_registry[notification_key] = {
            'notification_type': notification_type,
            'content_type': content_type,
            'serializer': serializer,
            'field_name': field_name
        }

    def __init__(self, *args, **kwargs):
        """
        Handles dynamic creation of nested fields; this cannot be done in the class method
        as `fields` doesn't exist at this point.
        """
        super().__init__(*args, **kwargs)
        for notification_gfk in self.notification_registry.values():
            field_name = notification_gfk['field_name']
            self.fields[field_name] = serializers.SerializerMethodField(method_name='get_empty_dict')

    def to_representation(self, instance):
        """
        Override the base to_representation() to add custom serialization of the `content_object`.

        Using the notification registry, this figures out which Serializer instance (e.g. Person or Status)
        to use to serialize the content_object, and which field to put the results into.
        """
        notification_key = (instance.app_label, instance.notification_type)

        try:
            notification_gfk = self.notification_registry[notification_key]
        except KeyError:
            raise RuntimeError("Trying to serialize a content_object using app_label=%s, "
                               "notification_type=%s, but that notification hasn't been registered.")

        field_name = notification_gfk['field_name']
        model_class = notification_gfk['content_type']
        serializer_class = notification_gfk['serializer']

        if instance and hasattr(instance, 'content_object') and isinstance(instance.content_object, model_class):
            # field_name is always included, but often blank.
            self.fields[field_name] = serializer_class(required=False, source='content_object')
        else:
            self.fields[field_name] = serializers.SerializerMethodField(method_name='get_empty_dict')

        return serializers.Serializer.to_representation(self, instance)

    
    def get_empty_dict(self, obj):
        return {}