import json

from rest_framework.test import APITestCase

from social.models import Account, PersonFollow, Person


class SocialTestCase(APITestCase):
    """
    Common setup for almost all test cases making use of social features like accounts
    and follows.
    """
    person_api_url = '/api/v1/accounts/{ID}'
    verify_credentials_url = '/api/v1/accounts/verify_credentials'
    update_credentials_url = '/api/v1/accounts/update_credentials'
    followers_url = '/api/v1/accounts/{ID}/followers'
    following_url = '/api/v1/accounts/{ID}/following'
    notifications_url = '/api/v1/notifications'

    
    def setUp(self):
        """
        Useful fixtures:
        - self.account: our test user's account.
        - self.person_a: our test user's person
        - self.person_b: a person on another instance that person_a follows
        - self.person_c: yet another person on yet another instance, with no follows
        """
        self.account = Account.objects.create(email='person_a@person_a.email.domain')
        self.account.set_password('test_pass')
        self.account.save(person_username='A')
        self.account.person.domain = 'test_instance.social'
        self.account.person.save()

        self.person_a = self.account.person

        # Person B has no Account; is a Person on another instance
        self.person_b = Person.objects.create(username='B', domain='someother.instance',
                                              preferred_username="Person B")

        PersonFollow.objects.create(follower=self.person_a,
                                    followed=self.person_b)

        # Yet another user on another instance, unrelated to the others
        self.person_c = Person.objects.create(username='C', domain='yetanother.social',
                                              preferred_username="Person C")


        # we also default to logging in the test user
        logged_in = self.login(self.person_a)
        self.assertTrue(logged_in)

    def login(self, person=None):
        if person is None:
            person = self.person_a
        return self.client.login(email=person.account.email, password='test_pass')

    def logout(self):
        return self.client.logout()

    def _post_json(self, url, data):
        return self.client.post(url, json.dumps(data), content_type='application/json', **{'wsgi.url_scheme': 'https'})

    def _patch_json(self, url, data):
        return self.client.patch(url, json.dumps(data), content_type='application/json', **{'wsgi.url_scheme': 'https'})

    def _create_people(self, number, *args, **kwargs):
        people = []
        for i in range(number):
            people.append(Person.objects.create(username='testperson%s' % i,
                                                domain='someother.instance',
                                                preferred_username="TestPerson %i" %i))
        return people
