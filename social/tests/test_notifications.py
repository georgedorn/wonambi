from social.tests import SocialTestCase
from social.models import Notification, Person
from social.serializers import NotificationSerializer, PersonSerializer


class TestNotificationsAPI(SocialTestCase):
    notifications_url = '/api/v1/notifications'
    notification_url = '/api/v1/notifications/{ID}'  # one notification by ID
    clear_notifications_url = "/api/v1/notifications/clear"  # m(

    def setUp(self):
        """
        Simple fixture data - clear all notifications that other fixtures may have created,
        but add two for person a.
        """
        super().setUp()
        Notification.objects.all().delete()
        self.note_b = Notification.objects.create(notification_type='follow', app_label='social',
                                    content_object=self.person_b, recipient=self.person_a, sender=self.person_b)
        self.note_c = Notification.objects.create(notification_type='follow', app_label='social',
                                    content_object=self.person_c, recipient=self.person_a, sender=self.person_c)


    def test_get_notifications(self):
        """
        Simple test of getting a collection of notifications about disparate types.
        """
        res = self.client.get(self.notifications_url)

        payload = res.json()
        self.assertEqual(len(payload), 2)

        # we also know order - newest to oldest
        self.assertEqual(payload[0]['type'], 'follow')
        self.assertEqual(payload[0]['account']['acct'], self.person_c.acct)
        self.assertEqual(payload[1]['type'], 'follow')
        self.assertEqual(payload[1]['account']['acct'], self.person_b.acct)

    def test_get_notifications_filtered_by_user(self):
        # Two notifications for other users - B follows C, C follows B
        Notification.objects.create(notification_type='follow', app_label='social',
                                    content_object=self.person_c, recipient=self.person_b, sender=self.person_c)
        Notification.objects.create(notification_type='follow', app_label='social',
                                    content_object=self.person_b, recipient=self.person_c, sender=self.person_b)

        # despite there being four notifications, we only see two belonging to person A
        res = self.client.get(self.notifications_url)

        payload = res.json()
        self.assertEqual(len(payload), 2)

    def test_get_notification_by_id(self):
        """
        GET a notification by ID
        """
        res = self.client.get(self.notification_url.format(ID=self.note_b.pk))
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(payload['type'], 'follow')
        self.assertEqual(payload['account']['acct'], self.person_b.acct)

    def test_clear_notifications(self):
        """
        POST to .../clear nukes all of the user's notifications
        """
        # Two notifications for other users - B follows C, C follows B
        Notification.objects.create(notification_type='follow', app_label='social',
                                    content_object=self.person_c, recipient=self.person_b, sender=self.person_c)
        Notification.objects.create(notification_type='follow', app_label='social',
                                    content_object=self.person_b, recipient=self.person_c, sender=self.person_b)

        self.assertEqual(Notification.objects.count(), 4)
        self.assertEqual(Notification.objects.filter(recipient=self.person_a).count(), 2)
        self.client.post(self.clear_notifications_url)  # Not testing dumb response

        self.assertEqual(Notification.objects.count(), 2)

    def test_exclude_types(self):
        """
        GET notifications allows ?exclude_types=(list of types)
        """
        # create another type
        NotificationSerializer.register_notification(app_label='social',
                                                     notification_type='poke',
                                                     content_type=Person,
                                                     serializer=PersonSerializer,
                                                     field_name='account')
        Notification.objects.create(notification_type='poke', app_label='social',
                                    content_object=self.person_b, recipient=self.person_a, sender=self.person_b)

        # get just the follows
        res = self.client.get(self.notifications_url, {'exclude_types': ['poke']})
        self.assertEqual(len(res.json()), 2)

        # get just the pokes
        res = self.client.get(self.notifications_url, {'exclude_types': ['follow']})
        self.assertEqual(len(res.json()), 1)

    def test_notification_pagination(self):
        Notification.objects.all().delete()
        
        notes = []
        for _ in range(25):
            notes.append(Notification.objects.create(app_label='social',
                                                     notification_type='follow',
                                                     content_object=self.person_b,
                                                     recipient=self.person_a,
                                                     sender=self.person_b))

        res = self.client.get(self.notifications_url, {'limit': '5'})
        payload = res.json()
        ids = [p['id'] for p in payload]
        self.assertEqual(len(ids), 5)  # we asked for 5, better have gotten them
        expected_ids = [note.pk for note in notes[-1:-6:-1]]  # last 5 ids in reverse order
        self.assertEqual(ids, expected_ids)

        url = res.wsgi_request.get_raw_uri()
        expected = '<{URL}&since_id={SINCE_ID}>; rel="prev", <{URL}&max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=notes[-1].id,
            BEFORE_ID=notes[-5].id
            )
        self.assertEqual(expected, res['Link'])

