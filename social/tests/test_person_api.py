from social.tests import SocialTestCase
from social.models import Person, PersonFollow
from rest_framework.utils.urls import remove_query_param


class TestPersonAPI(SocialTestCase):

    def test_get_person(self):
        res = self.client.get(self.person_api_url.format(ID=self.person_b.id))
        self.assertEqual(res.json()['acct'], self.person_b.acct)

    def test_verify_credentials(self):
        res = self.client.get(self.verify_credentials_url)
        self.assertEqual(res.json()['acct'], self.person_a.acct)

    def test_update_credentials(self):
        data = {'display_name': "New Name"}
        res = self._patch_json(self.update_credentials_url, data)
        self.assertEqual(res.status_code, 200)

        self.assertEqual(res.json()['acct'], 'A@test_instance.social')
        self.assertEqual(res.json()['display_name'], 'New Name')

        self.person_a.refresh_from_db()
        self.assertEqual(self.person_a.preferred_username, 'New Name')

    def test_update_credentials_no_username(self):
        """
        Should not be able to change `username` field via this endpoint.
        """
        data = {'username': "bad_username"}
        res = self._patch_json(self.update_credentials_url, data)
        self.assertEqual(res.json()['username'], self.person_a.username)

        self.person_a.refresh_from_db()
        self.assertEqual(self.person_a.acct, 'A@test_instance.social')

    def test_update_credentials_no_acct(self):
        data = {'acct': "bad_username@bad_instance.social"}
        res = self._patch_json(self.update_credentials_url, data)
        self.assertEqual(res.json()['username'], self.person_a.username)

        self.person_a.refresh_from_db()
        self.assertEqual(self.person_a.acct, 'A@test_instance.social')

    def test_get_followers_simple(self):
        """
        Person A follows Person B.  Person B requests a list of their followers.
        """
        res = self.client.get(self.followers_url.format(ID=self.person_b.pk))
        self.assertEqual(res.status_code, 200)

        payload = res.json()
        self.assertEqual(len(payload), 1)

        self.assertEqual(payload[0]['acct'], 'A@test_instance.social')

    def test_get_following_simple(self):
        """
        Person A follows Person B.  Person A requests a list of who they are following.
        """
        res = self.client.get(self.following_url.format(ID=self.person_a.pk))

        self.assertEqual(res.status_code, 200)

        payload = res.json()
        self.assertEqual(len(payload), 1)

        self.assertEqual(payload[0]['acct'], 'B@someother.instance')

    def test_get_followers_paginated_max_id(self):
        """
        Create more followers for Person B, get only a subset via `max_id`.
        """
        # create person_0 through person_4
        for i in range(5):
            p = Person.objects.create(username='person_%s' % i)
            PersonFollow.objects.create(followed=self.person_b, follower=p)

        # our arbitrary cutoff: request users before 'person_2'
        max_id = Person.objects.get(username='person_2').pk
        res = self.client.get(self.followers_url.format(ID=self.person_b.pk), {'max_id': str(max_id)})
        self.assertEqual(len(res.json()), 3) # person_a, person_0, person_1
        for person in res.json():
            self.assertTrue(person['id'] < max_id)

        url = res.wsgi_request.get_raw_uri()
        url = remove_query_param(url, 'max_id')
        url = remove_query_param(url, 'since_id')

        link_header = res['Link']
        
        # we expect 'since_id' to specify person_1, as it is the last person in the response
        expected = '<{URL}?since_id={SINCE_ID}>; rel="prev", <{URL}?max_id={BEFORE_ID}>; rel="next"'.format(
            URL=url,
            SINCE_ID=Person.objects.get(username='person_1').pk,
            BEFORE_ID=self.person_a.pk
        )

        self.assertEqual(link_header, expected)
