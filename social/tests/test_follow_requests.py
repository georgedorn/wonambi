"""
Tests for FollowRequestViewSet.
"""
from social.tests import SocialTestCase
from social.models import PersonFollow, PersonFollowRequest
from django.test.utils import CaptureQueriesContext


class TestFollowRequestsAPI(SocialTestCase):
    follow_requests_url = '/api/v1/follow_requests'
    request_authorize_url = '/api/v1/follow_requests/{ID}/authorize'
    request_reject_url = '/api/v1/follow_requests/{ID}/reject'
    
    def test_get_follow_requests(self):
        """
        Create some follow requests, retrieve them from API.
        """
        PersonFollow.objects.all().delete()
        
        PersonFollowRequest.objects.create(follower=self.person_b, followed=self.person_a)
        PersonFollowRequest.objects.create(follower=self.person_c, followed=self.person_a)

        from django import db

        with CaptureQueriesContext(db.connections[db.DEFAULT_DB_ALIAS]) as cqc:
            res = self.client.get(self.follow_requests_url)

        self.assertLessEqual(len(cqc.captured_queries), 4)  # expect: session, login, request.user, follower requests

        payload = res.json()
        self.assertEqual(len(payload), 2)

        # ordered by most recent - C
        self.assertEqual(payload[0]['acct'], self.person_c.acct)
        self.assertEqual(payload[1]['acct'], self.person_b.acct)

    def test_authorize_request(self):
        PersonFollow.objects.all().delete()
        
        PersonFollowRequest.objects.create(follower=self.person_b, followed=self.person_a)

        self.client.post(self.request_authorize_url.format(ID=self.person_b.pk))

        self.assertEqual(PersonFollow.objects.filter(follower=self.person_b, followed=self.person_a).exists(),
                         True)
        # Authorizing the request should delete the request after creating a real PersonFollow
        self.assertEqual(PersonFollowRequest.objects.filter(follower=self.person_b,
                                                            followed=self.person_a).exists(),
                         False)

    def test_reject_request(self):
        PersonFollow.objects.all().delete()
        
        PersonFollowRequest.objects.create(follower=self.person_b, followed=self.person_a)

        self.client.post(self.request_reject_url.format(ID=self.person_b.pk))

        # No follows, no requests
        self.assertEqual(PersonFollow.objects.filter(follower=self.person_b,
                                                     followed=self.person_a).exists(),
                         False)
        self.assertEqual(PersonFollowRequest.objects.filter(follower=self.person_b,
                                                            followed=self.person_a).exists(),
                         False)

    def test_authorize_request_without_request_fails(self):
        """
        Dumb test to assert you can't force somebody to follow you by approving a non-request.
        """
        PersonFollow.objects.all().delete()
        res = self.client.post(self.request_authorize_url.format(ID=self.person_b.pk))

        self.assertEqual(res.status_code, 404)

        # No follows, no requests
        self.assertEqual(PersonFollow.objects.filter(follower=self.person_b,
                                                     followed=self.person_a).exists(),
                         False)
        self.assertEqual(PersonFollowRequest.objects.filter(follower=self.person_b,
                                                            followed=self.person_a).exists(),
                         False)
