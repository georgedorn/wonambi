"""
Tests specifically for registering notification types and serializing bundles thereof.

Tests for serializing specific Notifications should be done in the respective apps, or a top-level
test module.
"""
from django.test.testcases import TestCase
from social.models import Person, Notification
from social.serializers import PersonSerializer, NotificationSerializer

class NotificationRegistryTestCase(TestCase):
    def setUp(self):
        # nuke existing notifications, as future apps will add more
        self.old_registry = NotificationSerializer.notification_registry
        NotificationSerializer.notification_registry = {}

    def tearDown(self):
        NotificationSerializer.notification_registry = self.old_registry

    def test_register_notification(self):
        NotificationSerializer.register_notification(app_label='social',
                                                     notification_type='follow',
                                                     content_type=Person,
                                                     serializer=PersonSerializer,
                                                     field_name='account')
        # now try to serialize a follow-like situation
        followee = Person.objects.create(username='followee', domain='test.instance')
        follower = Person.objects.create(username='follower', domain='test.instance')

        notification = Notification.objects.create(content_object=follower,
                                                   app_label='social',
                                                   notification_type='follow',
                                                   recipient=followee)

        serializer = NotificationSerializer(instance=notification)
        data = serializer.data
        self.assertEqual(data['type'], 'follow')
        self.assertEqual(data['account']['acct'], follower.acct)

#TODO:  Tests for avoiding collisions with app_label
#TODO:  Tests for sparse returns