from social.tests import SocialTestCase


class TestAccountSearch(SocialTestCase):
    
    accounts_search_url = "/api/v1/accounts/search"

    def test_simple_search(self):
        """
        Searching for 'person_b' should yield person_b.
        """
        res = self.client.get(self.accounts_search_url, {'q': self.person_b.username})
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        
        self.assertEqual(len(payload), 1)
        self.assertEqual(payload[0]['acct'], self.person_b.acct)
        
    def test_search_domain(self):
        """
        Searching by domain should work.
        """
        res = self.client.get(self.accounts_search_url, {'q': self.person_b.domain})
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        
        self.assertEqual(len(payload), 1)
        self.assertEqual(payload[0]['acct'], self.person_b.acct)

    def test_display_name(self):
        """
        Searching by preferred display name should also work.
        """
        res = self.client.get(self.accounts_search_url, {'q': self.person_b.preferred_username})
        self.assertEqual(res.status_code, 200)
        payload = res.json()
        
        self.assertEqual(len(payload), 1)
        self.assertEqual(payload[0]['acct'], self.person_b.acct)

    def test_search_limits(self):
        self._create_people(10)
        res = self.client.get(self.accounts_search_url, {'q': 'testperson', 'limit': '5'})

        self.assertEqual(res.status_code, 200)
        payload = res.json()
        self.assertEqual(len(payload), 5)
        for person in payload:
            self.assertTrue(person['username'].startswith('testperson'))

    def test_search_only_following(self):
        """
        Specifying ?following=true should limit results to only those accounts that the currently-logged-in
        user is following.
        """
        self._create_people(10)
        res = self.client.get(self.accounts_search_url, {'q': 'person', 'following': 'true'})
        payload = res.json()
        
        self.assertEqual(len(payload), 1)
        self.assertEqual(payload[0]['acct'], self.person_b.acct)

