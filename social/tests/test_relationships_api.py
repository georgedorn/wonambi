from social.tests import SocialTestCase
from social.models import PersonFollow, Person, PersonFollowRequest


class TestRelationshipAPIs(SocialTestCase):
    """
    Test case for various Person-to-Person relationship control endpoints.
    Many of these also use the Accounts endpoints.
    """
    follow_url = "/api/v1/accounts/{ID}/follow"
    unfollow_url = "/api/v1/accounts/{ID}/unfollow"
    relationships_url = '/api/v1/accounts/relationships'

    def test_follow(self):
        PersonFollow.objects.all().delete()  # start with fresh graph
        res = self.client.post(self.follow_url.format(ID=self.person_c.pk))
        self.assertEqual(res.status_code, 200)

        # Assumption: Relationship.id is the id of the target person, not logged-in person or other ID.
        self.assertEqual(res.json()['id'], self.person_c.pk)
        self.assertEqual(res.json()['following'], True)
        self.assertEqual(PersonFollow.objects.filter(followed=self.person_c, follower=self.person_a).exists(),
                         True)
        # A new notification to person_c should exist regarding this action
        notification = self.person_c.notifications.first()
        self.assertEqual(notification.notification_type, 'follow')
        self.assertEqual(notification.content_object, self.person_a)

    def test_unfollow(self):
        PersonFollow.objects.all().delete()  # start with fresh graph

        # make Person A follow person B
        PersonFollow.objects.create(followed=self.person_b, follower=self.person_a)

        # unfollow via API
        res = self.client.post(self.unfollow_url.format(ID=self.person_b.pk))

        # Assumption: Relationship.id is the id of the target person, not logged-in person or other ID.
        self.assertEqual(res.json()['id'], self.person_b.pk)
        self.assertEqual(res.json()['following'], False)
        self.assertEqual(PersonFollow.objects.filter(followed=self.person_b, follower=self.person_a).exists(),
                         False)

    def test_follow_request(self):
        """
        Try to follow a user with `locked` = True.
        """
        PersonFollow.objects.all().delete()  # start with fresh graph
        # Set Person C to locked
        self.person_c.locked = True
        self.person_c.save()

        res = self.client.post(self.follow_url.format(ID=self.person_c.pk))

        payload = res.json()
        self.assertEqual(payload['following'], False)
        self.assertEqual(payload['followed_by'], False)
        # Computed value indicating you've requested to follow the user
        self.assertEqual(payload['requested'], True)
        
        # A does not follow C (yet)
        self.assertEqual(PersonFollow.objects.filter(followed=self.person_c, follower=self.person_a).exists(),
                         False)
        # But has requested it.
        self.assertEqual(PersonFollowRequest.objects.filter(followed=self.person_c,
                                                            follower=self.person_a).exists(),
                         True)

    def test_unfollow_request(self):
        """
        POSTing to .../unfollow should remove existing follow requests, whether they've been accepted or not.
        """
        PersonFollow.objects.all().delete()  # start with fresh graph
        PersonFollowRequest.objects.create(follower=self.person_a, followed=self.person_c)
        self.client.post(self.unfollow_url.format(ID=self.person_c.pk))

        # But has requested it.
        self.assertEqual(PersonFollowRequest.objects.filter(followed=self.person_c,
                                                            follower=self.person_a).exists(),
                         False)

    def test_get_relationship_by_id(self):
        """
        Get a single relationship object by id.
        """
        res = self.client.get(self.relationships_url, {'id': self.person_b.pk})
        self.assertTrue(len(res.json()), 1)
        self.assertEqual(res.json()[0]['id'], self.person_b.pk)
        self.assertEqual(res.json()[0]['following'], True)

    def test_get_relationship_by_id_anonymous(self):
        """
        When not logged in, relationship data is still available, but
        fields pertaining to whether the user follows _you_ will always be false.
        """
        self.logout()
        res = self.client.get(self.relationships_url, {'id': self.person_b.pk})
        self.assertTrue(len(res.json()), 1)
        self.assertEqual(res.json()[0]['id'], self.person_b.pk)
        self.assertEqual(res.json()[0]['following'], False)
        self.assertEqual(res.json()[0]['followed_by'], False)
        self.assertEqual(res.json()[0]['requested'], False)


    def test_get_relationship_with_follow_request_by_id(self):
        PersonFollowRequest.objects.create(follower=self.person_a, followed=self.person_c)
        res = self.client.get(self.relationships_url, {'id': self.person_c.pk})
        self.assertTrue(len(res.json()), 1)
        self.assertEqual(res.json()[0]['id'], self.person_c.pk)
        self.assertEqual(res.json()[0]['following'], False)
        self.assertEqual(res.json()[0]['requested'], True)

    def test_get_relationships_by_ids(self):
        """
        Get multiple relationships, specifying the ids as a normal (WSGI-compatible) array.
        This request looks like:
        GET /api/v1/accounts/relationships?id=1&id=2
        """
        query_string = '?id=%s&id=%s' % (self.person_b.pk, self.person_c.pk)
        res = self.client.get(self.relationships_url + query_string)
        self.assertEqual(len(res.json()), 2)

        # Sanity-check response.  Remember A follows B, but not C.
        for relationship in res.json():
            self.assertTrue(relationship['id'] in (self.person_b.pk, self.person_c.pk))
            if relationship['id'] == self.person_b.pk:
                self.assertEqual(relationship['following'], True)
            else:
                self.assertEqual(relationship['following'], False)

    def test_get_relationships_by_ids_with_brackets(self):
        """
        Get multiple relationships, specifying the ids as a Rails/PHP-compatible array.
        This request looks like:
        GET /api/v1/accounts/relationships?id[]=1&id[]=2
        """
        query_string = '?id[]=%s&id[]=%s' % (self.person_b.pk, self.person_c.pk)
        res = self.client.get(self.relationships_url + query_string)
        self.assertEqual(len(res.json()), 2)

        # Sanity-check response.  Remember A follows B, but not C.
        for relationship in res.json():
            self.assertTrue(relationship['id'] in (self.person_b.pk, self.person_c.pk))
            if relationship['id'] == self.person_b.pk:
                self.assertEqual(relationship['following'], True)
            else:
                self.assertEqual(relationship['following'], False)
