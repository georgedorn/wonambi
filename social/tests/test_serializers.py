from django.test import TestCase
from social.models import Person
from social.serializers import PersonSerializer

class TestAcctSerializer(TestCase):

    def test_serializer_read(self):
        account = Person(username='test_user',
                         domain='test_domain',
                         preferred_username='preferred user name')
        serializer = PersonSerializer(account)
        self.assertEqual(serializer.data['acct'], 'test_user@test_domain')

    def test_serializer_write(self):
        account_details = {
                'acct': 'test_user@test_domain',
                'display_name': 'preferred user name',
                'bot': False
        }
        serializer = PersonSerializer(data=account_details)
        serializer.is_valid(raise_exception=True)
        new_person = serializer.save()
        self.assertEqual(new_person.username, 'test_user')
        self.assertEqual(new_person.domain, 'test_domain')

    def test_duplicate_person(self):
        account = Person.objects.create(username='test_user',
                                        domain='test_domain',
                                        preferred_username='preferred user name')
        account_details = {
            'acct': 'test_user@test_domain',
            'display_name': 'preferred user name',
            'bot': False
        }
        serializer = PersonSerializer(data=account_details)
        serializer.is_valid(raise_exception=True)
        new_person = serializer.save()
        self.assertEqual(new_person, account)
