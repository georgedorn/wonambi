"""
Tests of signup/registration workflows.
"""
from django.test.testcases import TestCase
from django.core.management import call_command
from social.models import Account
from django.test.utils import override_settings


@override_settings(DOMAIN='testdomain.inst')
class TestCreateUserCommand(TestCase):
    
    def test_create_user_non_superuser(self):
        call_command('createuser', '--noinput', person_username='testuser', email='test@test.email')
        user = Account.objects.get(email='test@test.email')
        self.assertEqual(user.person.acct, 'testuser@testdomain.inst')

    def test_create_user_cleanups(self):
        """
        Extra @ or spaces should be cleaned up during Person creation.
        """
        call_command('createuser', '--noinput', person_username='@testuser ', email='test@test.email')
        user = Account.objects.get(email='test@test.email')
        self.assertEqual(user.person.acct, 'testuser@testdomain.inst')

    def test_create_superuser(self):
        """
        Test to ensure ./manage.py createsuperuser still works; it doesn't create a Person,
        but we still need it.
        """
        call_command('createsuperuser', '--noinput', email='test@test.email')
        user = Account.objects.get(email='test@test.email')
        self.assertEqual(user.is_superuser, True)
