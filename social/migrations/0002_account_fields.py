# Generated by Django 2.1.1 on 2018-10-12 23:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='locked',
            field=models.BooleanField(default=False, help_text='True when Person cannot be followed without approval'),
        ),
        migrations.AddField(
            model_name='person',
            name='note',
            field=models.TextField(default='', help_text='Biography of user'),
        ),
        migrations.AddField(
            model_name='person',
            name='url',
            field=models.URLField(default=''),
        ),
    ]
