"""wonambi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include

from microblog.urls import router as microblog_router
from microblog.views import mastodon_home

from social.urls import router as social_router

from oauth.urls import router as oauth_router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(social_router.urls)),
    path('api/v1/', include(microblog_router.urls)),
    path(r'oauth/', include('oauth.urls', namespace='oauth2_provider')),
    path('api/v1/', include(oauth_router.urls)),
    path('accounts/', include('django.contrib.auth.urls')),
    path('web/getting-started/', mastodon_home, name='mastodon_home'),
]
