"""
Mixins for app-agnostic models, such as timestamps.
"""
from django.db import models


class TimestampedModel(models.Model):
    """
    Generic mixin for adding created_at / updated_at fields.
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True