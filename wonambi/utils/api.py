"""
Utility methods for use when implementing APIs.
"""

class PermissionsPerMethodMixin(object):
    def get_permissions(self):
        """
        Allows overriding default permissions by using @permission_classes, even with @action methods.
        """
        view = getattr(self, self.action)
        if hasattr(view, 'permission_classes'):
            return [permission_class() for permission_class in view.permission_classes]
        return super().get_permissions()


def get_boolean_query_param(request, param):
    """
    Helper method to accept boolean-like parameters in the query string and
    return a proper Python boolean.
    """
    value = request.query_params.get(param, False)

    if value in (1, '1', 'true', 'True', True):
        return True
    return False
