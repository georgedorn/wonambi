from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param, remove_query_param


class LiveResponseHeaderPagination:
    """
    Implements Mastodon-style pagination:
    - pagination details contained in the 'link' header of the response
    - pagination controlled via query params:
    -- 'since_id' to return objects with ID greater
    -- 'max_id' to return objects with ID less than (read as 'before_id')
    -- 'limit' to change number of objects returned.  default 20, max 40, override in child class
    """
    # Not a subclass of DRF's pagination as it is too custom.
    # Response header looks like this:
    # Status API:
    # link: <https://social.coop/api/v1/timelines/home?max_id=100860196884700824>; rel="next",
    # <https://social.coop/api/v1/timelines/home?since_id=100860464513978134>; rel="prev"
    # Accounts API:
    # link: <https://social.coop/api/v1/accounts/:id/following?max_id=55231>; rel="next", 
    # <https://social.coop/api/v1/accounts/:id/following?since_id=61524>; rel="prev"
    
    since_id_qp = 'since_id'  # name of query param for since_id
    before_id_qp = 'max_id'   # name of query param for before_id
    limit_qp = 'limit'

    def __init__(self, request, queryset, default_limit=20, default_max_limit=40):
        self.default_limit = default_limit
        self.default_max_limit = default_max_limit
        self.request = request
        self.queryset = queryset

    @classmethod
    def _generate_paginated_response(cls, request, queryset, serializer_class, **kwargs):
        """
        Boilerplate reduction; once an API's queryset has been calculated, generating the paginated
        response is almost always the same process.

        This could be refactored into a base API class.
        """
        paginator = cls(request, queryset, **kwargs)
        paginated_statuses = paginator.apply_pagination()

        serializer = serializer_class(paginated_statuses, many=True, context={'request': request})

        return paginator.get_paginated_response(serializer.data)

    def apply_pagination(self):
        """
        Universal method to apply query parameter filters to the provided queryset
        """
        queryset = self.queryset
        query_params = self.request.query_params
        if self.since_id_qp in query_params:
            queryset = queryset.filter(id__gt=int(query_params.get(self.since_id_qp)))
        if self.before_id_qp in query_params:
            queryset = queryset.filter(id__lt=int(query_params.get(self.before_id_qp)))

        limit = int(query_params.get(self.limit_qp, self.default_limit))
        limit = min(limit, self.default_max_limit)

        self.response_data = queryset[0:limit]

        # First and last are for this specific response, not all data, so dig them out of the response data
        # Also need to convert to list as .first() and .last() can't be used after slicing
        response_list = list(self.response_data)
        first = response_list[0] if response_list else None
        last = response_list[-1] if response_list else None
        
        self.max_id = first.id if first else None
        self.min_id = last.id if last else None

        return self.response_data

    def get_paginated_response(self, data):
        """
        Calculate link header and return a Response containing provided data and headers.
        """
        # some parts from https://github.com/tbeadle/django-rest-framework-link-header-pagination/blob/master/drf_link_header_pagination/__init__.py
        next_url = self.get_next_link()
        previous_url = self.get_previous_link()

        links = []
        for url, label in (
            (previous_url, 'prev'),
            (next_url, 'next'),
        ):
            if url is not None:
                links.append('<{}>; rel="{}"'.format(url, label))

        headers = {'Link': ', '.join(links)} if links else {}

        return Response(data, headers=headers)

    def get_next_link(self):
        url = self.request.build_absolute_uri()
        url = replace_query_param(url, self.before_id_qp, self.min_id)
        url = remove_query_param(url, self.since_id_qp)
        return url

    def get_previous_link(self):
        url = self.request.build_absolute_uri()
        url = remove_query_param(url, self.before_id_qp)
        url = replace_query_param(url, self.since_id_qp, self.max_id)
        return url
